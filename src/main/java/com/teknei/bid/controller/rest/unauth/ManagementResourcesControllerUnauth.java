package com.teknei.bid.controller.rest.unauth;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.bid.controller.rest.ManagementResourcesController;
import com.teknei.bid.dto.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/unauth/management")
@CrossOrigin
public class ManagementResourcesControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(ManagementResourcesControllerUnauth.class);
    @Autowired
    private ManagementResourcesController controller;

    @RequestMapping(value = "/admin/usua", method = RequestMethod.GET)
    public ResponseEntity<List<BidUsuaDTO>> findAllActive() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAllActive() }");
        return controller.findAllActive();
    }

    @RequestMapping(value = "/admin/usua", method = RequestMethod.POST)
    public ResponseEntity<BidUsuaDTO> insertUsua(@RequestBody BidUsuaDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{insertUsua() }");
        return controller.insertUsua(dto, request);
    }

    @RequestMapping(value = "/admin/usua", method = RequestMethod.PUT)
    public ResponseEntity<BidUsuaDTO> modifyUsua(@RequestBody BidUsuaDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{modifyUsua() }");
        return controller.modifyUsua(dto, request);
    }
    
    /**
     * Servicio para la modificacion de contraseña 
     * @author AJGD
     * @param dto tipo oper 0 = vigencia, 1 = recuperacion;
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/usua/updatePassword", method = RequestMethod.PUT)
    public ResponseEntity<String> updatePassword(@RequestBody BidUpdatePassUsuaDTO dto, HttpServletRequest request) {
    	log.info("AJGD: "+this.getClass().getName()+".{updatePassword() } :oper:"+dto.getTipoOper());
        return controller.updatePassword(dto, request);
    }

    @RequestMapping(value = "/admin/usua/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidUsuaDTO> deleteUsua(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{deleteUsua() }");
        return controller.deleteUsua(id);
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDTO>> findAllEmprActive() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAllEmprActive() }");
        return controller.findAllEmprActive();
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.POST)
    public ResponseEntity<BidEmprDTO> saveEmpr(@RequestBody BidEmprDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveEmpr() }");
        return controller.saveEmpr(dto, request);
    }

    @RequestMapping(value = "/admin/empr", method = RequestMethod.PUT)
    public ResponseEntity<BidEmprDTO> updateEmpr(@RequestBody BidEmprDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateEmpr() }");
        return controller.updateEmpr(dto, request);
    }

    @RequestMapping(value = "/admin/empr/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidEmprDTO> deleteEmpr(@PathVariable Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{deleteEmpr() }");
        return controller.deleteEmpr(id, request);
    }

    @RequestMapping(value = "/admin/disp", method = RequestMethod.GET)
    public ResponseEntity<List<BidDispDTO>> findAllDispActive() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAllDispActive() }");
        return controller.findAllDispActive();
    }

    @RequestMapping(value = "/admin/disp", method = RequestMethod.POST)
    public ResponseEntity<BidDispDTO> createDisp(@RequestBody BidDispDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{createDisp() }");
        return controller.createDisp(dto, request);
    }

    @RequestMapping(value = "/admin/disp", method = RequestMethod.PUT)
    public ResponseEntity<BidDispDTO> updateDisp(@RequestBody BidDispDTO dto, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateDisp() }");
        return controller.updateDisp(dto, request);
    }

    @RequestMapping(value = "/admin/disp/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BidDispDTO> deleteDisp(@PathVariable Long id, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{deleteDisp() }");
        return controller.deleteDisp(id, request);
    }

    @ApiOperation(value = "Assigns the relation between device and company", notes = "Only idCompany and idDisp values of the request are needed, others can be zero", response = Boolean.class)
    @RequestMapping(value = "/assign/companyDevice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyDevice(@RequestBody BidAssignmentRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{assignCompanyDevice() }");
        return controller.assignCompanyDevice(requestDTO, request);
    }

    @ApiOperation(value = "Assigns the relation between user and company", notes = "Only idCompany and idCustomer values of the request are needed, others can be zero", response = Boolean.class)
    @RequestMapping(value = "/assign/companyUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyUser(@RequestBody BidAssignmentRequestDTO requestDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{assignCompanyUser() }");
        return controller.assignCompanyUser(requestDTO, request);
    }

    @ApiOperation(value = "Finds the information of the assignments related to the user", response = List.class)
    @RequestMapping(value = "/assign/companyUser/{idUser}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprUsua>> findCompanyFromUser(@PathVariable Long idUser) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findCompanyFromUser() }");
        return controller.findCompanyFromUser(idUser);
    }

    @ApiOperation(value = "Finds the information on assignments related to the device", response = List.class)
    @RequestMapping(value = "/assign/companyDevice/{idDevice}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDisp>> findCompanyFromDevice(@PathVariable Long idDevice) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findCompanyFromDevice() }");
        return controller.findCompanyFromDevice(idDevice);
    }
}
