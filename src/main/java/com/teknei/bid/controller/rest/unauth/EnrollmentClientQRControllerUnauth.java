package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentClientQRController;
import com.teknei.bid.dto.ClieQRDTO;
import com.teknei.bid.dto.ClieQRDTORequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/client/qr")
@CrossOrigin
public class EnrollmentClientQRControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentClientQRControllerUnauth.class);
    @Autowired
    private EnrollmentClientQRController controller;

    @RequestMapping(value = "/customerQR", method = RequestMethod.POST)
    public ResponseEntity<ClieQRDTO> generateQRCode(@RequestBody ClieQRDTORequest qrdtoRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateQRCode(POST) }");
        return controller.generateQRCode(qrdtoRequest, request);
    }

    @RequestMapping(value = "/customerQR/image", method = RequestMethod.POST)
    public ResponseEntity<byte[]> generateQRImageCode(@RequestBody ClieQRDTORequest qrdtoRequest, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateQRImageCode(POST) }");
        return controller.generateQRImageCode(qrdtoRequest, request);
    }

    @RequestMapping(value = "/customerQR/{qrCode}", method = RequestMethod.GET)
    public ResponseEntity<String> validateQRCode(@PathVariable String qrCode, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{validateQRCode(GET) }");
        return controller.validateQRCode(qrCode, request);
    }

}
