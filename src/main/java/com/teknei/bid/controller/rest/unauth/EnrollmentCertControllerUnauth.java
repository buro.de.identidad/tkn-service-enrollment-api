package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentCertController;
import com.teknei.bid.dto.OperationIdDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/cert")
@CrossOrigin
public class EnrollmentCertControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentCertControllerUnauth.class);
    @Autowired
    private EnrollmentCertController controller;

    @RequestMapping(value = "/usercert", method = RequestMethod.POST)
    public ResponseEntity<String> generateCert(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateCert(POST) }");
        return controller.generateCert(operationIdDTO, request);
    }

    @RequestMapping(value = "/usercert/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateCert(@PathVariable Long id){
    	//log.info("lblancas: "+this.getClass().getName()+".{validateCert(GET) }");
        return controller.validateCert(id);
    }

}
