package com.teknei.bid.controller.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/rest/v3")
@CrossOrigin
public class ExampleController {

    @GetMapping
    public Map<String, String> example() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "value");
        return map;
    }
}
