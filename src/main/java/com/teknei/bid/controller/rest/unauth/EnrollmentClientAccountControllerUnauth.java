package com.teknei.bid.controller.rest.unauth;

import com.teknei.bid.controller.rest.EnrollmentClientAccountController;
import com.teknei.bid.dto.BidClieCtaDest;
import com.teknei.bid.dto.BidClientAccountDestinyDTO;
import com.teknei.bid.dto.BidCreditInstitutionRequest;
import com.teknei.bid.dto.BidInstCred;
import com.teknei.bid.dto.BidReasonFingers;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/unauth/enrollment/client/account")
@CrossOrigin
public class EnrollmentClientAccountControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentClientAccountControllerUnauth.class);
    @Autowired
    private EnrollmentClientAccountController controller; 

    
    @ApiOperation(value = "getPrivacyNotice")
    @RequestMapping(value = "/getPrivacyNotice", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getPrivacyNotice(){
    	log.info("AJGD: "+this.getClass().getName()+".{getPrivacyNotice(1) }");
        return controller.getPrivacyNotice();
    }
    
    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/getReasonFingers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidReasonFingers>> getReasonFingers() 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{getReasonFingers(1) }");
        return controller.getReasonFingers();
    }
    
    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/getUsuarioLogin/{nombre}/{paterno}/{materno}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUsuarioLogin(@PathVariable String nombre,@PathVariable String paterno,@PathVariable String materno){
    	log.info("lblancas: "+this.getClass().getName()+".{getUsuarioLogin("+nombre+","+paterno+","+materno+") }");
    	return controller.getUsuarioLogin( nombre,  paterno,  materno); 
    }
    
    @ApiOperation(value = "Gets all credit institution records", response = String.class)
    @RequestMapping(value = "/validaUsuario/{usuario}/{password}", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "User or password not provided"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 200, message = "Request sucsesfulls"),
            @ApiResponse(code = 400, message = "User not found")
    })
    public ResponseEntity<String> validaUsuario(@PathVariable String usuario,@PathVariable String password) {
    	log.info("lblancas: "+this.getClass().getName()+".{validaUsuario(1) }");    	
        return controller.validaUsuario(usuario,password);
    }
    
    @ApiOperation(value = "Finds the accounts related to the customer")
    @RequestMapping(value = "/accountDestiny/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccounts(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccounts() }");
        return controller.findRelatedAccounts(idClient);
    }

    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/accountDestinyInactive/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccountsInactive(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccountsInactive() }");
        return controller.findRelatedAccountsInactive(idClient);
    }

    @ApiOperation(value = "Saves or updates account destiny information for the related customer. Identifiers are required, boolean fields are recommended for expected behavior", response = BidClieCtaDest.class)
    @RequestMapping(value = "/accountDestiny", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidClieCtaDest> saveCtaDest(@RequestBody BidClientAccountDestinyDTO request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveCtaDest() }");
        return controller.saveCtaDest(request);
    }

    @ApiOperation(value = "Gets all credit institution records", response = List.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.GET)
    public ResponseEntity<List<BidInstCred>> findInstCred() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findInstCred() }");
        return controller.findInstCred();
    }
    

    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidInstCred> saveInstCred(@RequestBody BidCreditInstitutionRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveInstCred() }");
        return controller.saveInstCred(request);
    }

    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitutionList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> saveInstCredList(@RequestBody List<BidInstCred> bankList) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveInstCred() }");
        return controller.saveInstCredList(bankList);
    }

}
