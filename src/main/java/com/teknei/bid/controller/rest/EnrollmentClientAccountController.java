package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.BidClieCtaDest;
import com.teknei.bid.dto.BidClientAccountDestinyDTO;
import com.teknei.bid.dto.BidCreditInstitutionRequest;
import com.teknei.bid.dto.BidInstCred;
import com.teknei.bid.dto.BidReasonFingers;
import com.teknei.bid.oauth.domain.User;
import com.teknei.bid.oauth.services.UserRepository;
import com.teknei.bid.service.remote.CustomerClient;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/client/account")
@CrossOrigin
public class EnrollmentClientAccountController {

    @Autowired
    private CustomerClient customerClient;
    private static final Logger log = LoggerFactory.getLogger(EnrollmentClientAccountController.class);

    @Autowired
    private UserRepository userRepository;

    @ApiOperation(value = "Finds the accounts related to the customer")
    @RequestMapping(value = "/accountDestiny/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccounts(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccounts() }");
      
    	try {
            return customerClient.findRelatedAccounts(idClient);
        } catch (Exception fe) {
            return handleFailure(fe);
        }
    }

    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/accountDestinyInactive/{idClient}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidClieCtaDest>> findRelatedAccountsInactive(@PathVariable Long idClient) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findRelatedAccountsInactive() }");
        try {
            return findRelatedAccountsInactive(idClient);
        } catch (Exception e) {
            return handleFailure(e);
        }
    }

    @ApiOperation(value = "Saves or updates account destiny information for the related customer. Identifiers are required, boolean fields are recommended for expected behavior", response = BidClieCtaDest.class)
    @RequestMapping(value = "/accountDestiny", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidClieCtaDest> saveCtaDest(@RequestBody BidClientAccountDestinyDTO request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveCtaDest() }");
        try {
            return customerClient.saveCtaDest(request);
        } catch (Exception e) {
            return handleFailure(e);
        }
    }

    @ApiOperation(value = "Gets all credit institution records", response = List.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.GET)
    public ResponseEntity<List<BidInstCred>> findInstCred() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findInstCred() }");
        try {
            return customerClient.findInstCred();
        } catch (Exception e) {
            return handleFailure(e);
        }
    }
    
    //TODO prueba ajgd valida usuario--->>>
    @ApiOperation(value = "Gets all credit institution records", response = String.class)
    @RequestMapping(value = "/validaUsuario/{usuario}/{password}", method = RequestMethod.GET)
    @ApiResponses(value = {
               @ApiResponse(code = 500, message = "Internal server error"),
               @ApiResponse(code = 200, message = "Request sucsesfulls")
    })
    public ResponseEntity<String> validaUsuario(@PathVariable String usuario,@PathVariable String password) {        
    	log.info("lblancas: "+this.getClass().getName()+".{validaUsuario(2) }");
		try {
			return customerClient.validaUsuario(usuario, password);
		} catch (FeignException e) {
			return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(),HttpStatus.INTERNAL_SERVER_ERROR);			
		}
    }
    
	@ApiOperation(value = "Get Privacy Notice", response = String.class)
	@RequestMapping(value = "/getPrivacyNotice", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Internal server error"),
			@ApiResponse(code = 200, message = "Request sucsesfulls") })
	public ResponseEntity<String> getPrivacyNotice() {
		log.info("AJGD: " + this.getClass().getName() + ".{getPrivacyNotice(2) }");
		try {
			return customerClient.getPrivacyNotice();
		} catch (FeignException e) {
			return new ResponseEntity<>(e.getMessage().split("content:")[1].trim(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
     
	/**
	 * Genera un nombre de usuario con la estructura NNPPMM99
	 * @param nombre
	 * @param paterno
	 * @param materno
	 * @return
	 */
    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/getUsuarioLogin/{nombre}/{paterno}/{materno}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUsuarioLogin(@PathVariable String nombre,@PathVariable String paterno,@PathVariable String materno) {
    	log.info("lblancas: "+this.getClass().getName()+".{getUsuarioLogin(2) }");
        try {
        	return customerClient.getUsuarioLogin( nombre,  paterno,  materno);
        } catch (Exception e) {
            return handleFailure(e);
        }
    }
    
    
    
    @ApiOperation(value = "Finds the accounts related to the customer, only inactive ones")
    @RequestMapping(value = "/getReasonFingers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidReasonFingers>> getReasonFingers() 
    {
    	log.info("lblancas: "+this.getClass().getName()+".{getReasonFingers(2) }");
        try {
            return customerClient.getReasonFingers();
        } catch (Exception e) {
            return handleFailure(e);
        }
    }
    
    
    

    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitution", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidInstCred> saveInstCred(@RequestBody BidCreditInstitutionRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{saveInstCred() }");
        try {
            return customerClient.saveInstCred(request);
        } catch (Exception e) {
            return handleFailure(e);
        }
    }
    @ApiOperation(value = "Saves or updates new credit institution based on request values. Send only name values when new record is needed, all fields are required when update is request", response = BidInstCred.class)
    @RequestMapping(value = "/creditInstitutionList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> saveInstCredList(@RequestBody List<BidInstCred> bankList) {
    	log.info("AJGD: "+this.getClass().getName()+".{saveInstCred() }");
        try {
            return customerClient.saveInstCred(bankList);
        } catch (Exception e) {
            return handleFailure(e);
        }
    }

    public ResponseEntity handleFailure(Exception e) {
    	//log.info("lblancas: "+this.getClass().getName()+".{handleFailure() }");
        if (e instanceof FeignException) {
            FeignException fe = (FeignException) e;
            switch (fe.status()) {
                case 422:
                    return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
                case 404:
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                default:
                    return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        log.error("No http feign error detected: {}", e.getMessage());
        return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
    }

}
