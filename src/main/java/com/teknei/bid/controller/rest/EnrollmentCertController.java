package com.teknei.bid.controller.rest;

import com.teknei.bid.controller.rest.util.crypto.TokenUtils;
import com.teknei.bid.dto.CertKey;
import com.teknei.bid.dto.ClientDetailDTO;
import com.teknei.bid.dto.OperationIdDTO;
import com.teknei.bid.service.remote.BiometricClient;
import com.teknei.bid.service.remote.ContractClient;
import com.teknei.bid.service.remote.CustomerClient;
import feign.FeignException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/v3/enrollment/cert")
@CrossOrigin
public class EnrollmentCertController {

    @Autowired
    private ContractClient certClient;
    @Autowired
    private CustomerClient customerClient;
    @Autowired
    private BiometricClient biometricClient;
    @Autowired
    private TokenUtils tokenUtils;

    private static final Logger log = LoggerFactory.getLogger(EnrollmentCertController.class);

    @RequestMapping(value = "/usercert/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> validateCert(@PathVariable Long id){
    	//log.info("lblancas: "+this.getClass().getName()+".{validateCert() }");
        JSONObject jsonObject = new JSONObject();
        try {
            ResponseEntity<String> responseEntity = certClient.getCert(id);
            JSONObject jsonSerial = new JSONObject(responseEntity.getBody());
            return new ResponseEntity<>(jsonSerial.toString(), HttpStatus.OK);
        } catch (FeignException e) {
            jsonObject.put("serial", "NA");
            if(e.status() == 404){
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }catch (Exception e){
            log.error("Error unexpected for getting user cert details: {}", e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/usercert", method = RequestMethod.POST)
    public ResponseEntity<String> generateCert(@RequestBody OperationIdDTO operationIdDTO, HttpServletRequest request) {
    	//log.info("lblancas: "+this.getClass().getName()+".{generateCert() }");
        try {
            String username = (String) tokenUtils.getExtraInfo(request).get(TokenUtils.DETAILS_USERNAME_MAP_NAME);
            //log.info("lblancas: "+this.getClass().getName()+".{customerClient.findDetailId(operationIdDTO.getOperationId() inicia }");
            ResponseEntity<ClientDetailDTO> responseEntity = customerClient.findDetailId(operationIdDTO.getOperationId());
            //log.info("lblancas: "+this.getClass().getName()+".{customerClient.findDetailId(operationIdDTO.getOperationId()  termina Operation ["+operationIdDTO.getOperationId()+"] }");
            
            //log.info("lblancas: "+this.getClass().getName()+".{biometricClient.getHashForCustomer(String.valueOf(operationIdDTO.getOperationId())  inicia }");
            ResponseEntity<String> responseHash = biometricClient.getHashForCustomer(String.valueOf(operationIdDTO.getOperationId()));
            //log.info("lblancas: "+this.getClass().getName()+".{biometricClient.getHashForCustomer(String.valueOf(operationIdDTO.getOperationId())  termina }");
            JSONObject jsonHash = new JSONObject(responseHash.getBody());
            String hash = jsonHash.getString("hash");
            ClientDetailDTO dto = responseEntity.getBody();
            log.info("scanId   :"+dto.getScanId());
            log.info("documentId   :"+dto.getDocumentId());
            log.info("dire   :"+dto.getDire());
            log.info("personalNumber   :"+dto.getPersonalNumber());
            log.info("mrz   :"+dto.getMrz());
            log.info("ocr   :"+dto.getOcr());
            log.info("vig   :"+dto.getVig());
            log.info("curpDocument   :"+dto.getCurpDocument());

            CertKey certKey = new CertKey();
            certKey.setCustomerId(dto.getId().toString());
            certKey.setCustomerMail(dto.getEmail());
            certKey.setCustomerName(dto.getName() + "_" + dto.getSurnameFirst() + "_" + dto.getSurnameLast());
            certKey.setKey(hash);
            certKey.setUsernameRequesting(username);
            //log.info("lblancas: "+this.getClass().getName()+".{certClient.generateCert(certKey)  inicia }");
            ResponseEntity<String> responseCert = certClient.generateCert(certKey);
            //log.info("lblancas: "+this.getClass().getName()+".{certClient.generateCert(certKey)  termina }");
            return new ResponseEntity<>(new JSONObject().put("statusMessage", responseCert.getBody()).toString(), HttpStatus.OK);
        } catch (FeignException fe) {
            switch (fe.status()) {
                case 403:
                    return new ResponseEntity(new JSONObject().put("statusMessage", fe.getMessage()).toString(), HttpStatus.FORBIDDEN);
                case 409:
                    return new ResponseEntity(new JSONObject().put("statusMessage", fe.getMessage()).toString(), HttpStatus.CONFLICT);
                default:
                    return new ResponseEntity(new JSONObject().put("statusMessage", fe.getMessage()).toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error retrieving customer details, unable to generate cert: {}", e.getMessage());
            return new ResponseEntity<>(new JSONObject().put("Error", e.getMessage()).toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
