package com.teknei.bid.controller.rest.unauth;
import org.springframework.http.HttpStatus;
import com.teknei.bid.controller.rest.EnrollmentClientDetailController;
import com.teknei.bid.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@RestController
@RequestMapping(value = "/rest/unauth/enrollment/client/detail")
@CrossOrigin
public class EnrollmentClientDetailControllerUnauth {
	private static final Logger log = LoggerFactory.getLogger(EnrollmentClientDetailControllerUnauth.class);
    @Autowired
    private EnrollmentClientDetailController controller;

    @ApiOperation(value = "Finds catalogo de tipo de cliente", response = List.class)
    @RequestMapping(value = "/catalogoTipoCliente", method = RequestMethod.GET)
    public ResponseEntity<List<BidTipoClienteDTO>> getCatalogoTipoCliente()  {
    	//log.info("lblancas: "+this.getClass().getName()+".{findOpenedForToday(GET) }");
        return controller.getCatalogoTipoCliente();
    }
    
    @ApiOperation(value = "Verifies if the customer has any previous record associated", notes = "The response should contain only one record in the array", response = BidIfeValidationDTO.class)
    @RequestMapping(value = "/validate/information/ine", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<BidIfeValidationDTO>> findValidation(@RequestBody BidIfeValidationRequestDTO validationRequestDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findValidation(POST) }");
        return controller.findValidation(validationRequestDTO);
    }
    
    @ApiOperation(value = "Verifies if the customer has any previous record associated", notes = "The response should contain only one record in the array", 
    			response = BidServiceConfiguration.class)
    @RequestMapping(value = "/serviceConfiguration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
    	produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BidServiceConfiguration> getServiceConfigurationUn() 
    {
    	//log.info("lblancas:[1] "+this.getClass().getName()+".{getServiceConfiguration() }");
    	return controller.getServiceConfiguration();
    }

    @ApiOperation(value = "Verifies if the customer has the biometric step completed", notes = "Returns 200 if ok, 404 if not found", response = Boolean.class)
    @RequestMapping(value = "/verify/biometric/{operationId}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> findVerificationForCandidate(@PathVariable Long operationId) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findVerificationForCandidate(GET) }");
        return controller.findVerificationForCandidate(operationId);
    }


    @ApiOperation(value = "Finds the current processes opened for today", response = List.class)
    @RequestMapping(value = "/search/openedForToday", method = RequestMethod.GET)
    public ResponseEntity<List<ClientBiometricCandidateDTO>> findOpenedForToday() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findOpenedForToday(GET) }");
        return controller.findOpenedForToday();
    }


    @ApiOperation(value = "Finds the current processes opened for today", response = List.class)
    @RequestMapping(value = "/search/contractsForToday", method = RequestMethod.GET)
    public ResponseEntity<List<ClientBiometricCandidateDTO>> findOpenForContractProcess() {
    	//log.info("lblancas: "+this.getClass().getName()+".{findOpenForContractProcess(GET) }");
        return controller.findOpenForContractProcess();
    }


    @ApiOperation(value = "Gets the data in detail from the search parameters send as JSON. The response will be a JSON string as {nombre, apellidoPaterno, apellidoMaterno, curpCapturado, curpIdentificado, direccion, scanId, documentManagerId, mrz, ocr, vig}", response = String.class)
    @RequestMapping(value = "/detail", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findDetail(@RequestBody String searchDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        return controller.findDetail(searchDTO);
    }
    @ApiOperation(value = "Gets the data in detail from the search parameters send as JSON. The response will be a JSON string as {nombre, apellidoPaterno, apellidoMaterno, curpCapturado, curpIdentificado, direccion, scanId, documentManagerId, mrz, ocr, vig}", response = String.class)
    @RequestMapping(value = "/detailByCurpAndType", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findDetailbyCrpAndTp(@RequestBody String searchDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findAccomplishmnet() }");
        return controller.findDetailbyCrpAndTp(searchDTO);
    }
    
    @ApiOperation(value = "Finds the current step for the requested operation")
    @RequestMapping(value = "/step", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<String> findStep(@RequestBody SearchDTO searchDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findStep(POST) }");
        return controller.findStep(searchDTO);
    }


    @ApiOperation(value = "Search the customer related for the curp. The answer will be like {nombre, curp, apellidoPaterno, apellidoMaterno, email, fecha, id}")
    @RequestMapping(value = "/search/customer/{curp}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> searchByData(@PathVariable String curp) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByData(GET) }");
        return controller.searchByData(curp);
    }

    @ApiOperation(value = "Search the customer related for the id. The answer will be like {nombre, curp, apellidoPaterno, apellidoMaterno, email, fecha, id}")
    @RequestMapping(value = "/search/customerId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> searchByDataId(@PathVariable Long id) {
    	//log.info("lblancas: "+this.getClass().getName()+".{searchByDataId(GET) }");
        return controller.searchByDataId(id);
    }

    @ApiOperation(value = "Search the related Timestamps values for the requested CURP", response = DetailTSRecordResponseDTO.class)
    @RequestMapping(value = "/search/customer/ts/{operationId}/{curp}", method = RequestMethod.GET)
    public ResponseEntity<DetailTSRecordResponseDTO> findDetailTS(@PathVariable String operationId, @PathVariable String curp) {
    	//log.info("lblancas: "+this.getClass().getName()+".{findDetailTS(GET) }");
        return controller.findDetailTS(operationId, curp);
    }

    @ApiOperation(value = "Updates the customer record based on requested values. The id must be a valid identifier", notes = "If error ocurrs, the detail of the error will be a json in 'obs' field inside the response", response = ClientDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the customer could be updated"),
            @ApiResponse(code = 422, message = "If the customer fails in the update process"),
            @ApiResponse(code = 400, message = "Bad request, usually 'id' is missing")
    })
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<ClientDTO> updateClient(@RequestBody ClientDTO clientDTO) {
    	//log.info("lblancas: "+this.getClass().getName()+".{updateClient(PUT) }");
        return controller.updateClient(clientDTO);
    }


}
