package com.teknei.bid.controller.rest.util.crypto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Component
public class TokenUtils {

    private BearerTokenExtractor tokenExtractor;
    //@Autowired
    private TokenStore tokenStore;

    public static final Logger log = LoggerFactory.getLogger(TokenUtils.class);
    public static final String DETAILS_USERNAME_MAP_NAME = "user_name";
    public static final String DETAILS_SCOPE_MAP_NAME = "scope";
    public static final String DETAILS_ORGANIZATION_MAP_NAME = "organization";
    public static final String DETAILS_AUTHORITIES_MAP_NAME = "authorities";
    public static final String DETAILS_CLIENT_ID_MAP_NAME = "client_id";


    @PostConstruct
    private void postConstruct() {
        tokenExtractor = new BearerTokenExtractor();
    }

    public Map<String, Object> getExtraInfo(HttpServletRequest request) {
        String username = "NA";
        try {
            Authentication auth = tokenExtractor.extract(request);
            OAuth2Authentication oauth = tokenStore.readAuthentication(auth.getPrincipal().toString());
            username = oauth.getPrincipal().toString();
            Map<String, Object> details = (Map<String, Object>) oauth.getDetails();
            if (details == null) {
                details = new HashMap<>();
                details.put(DETAILS_USERNAME_MAP_NAME, oauth.getPrincipal().toString());
                log.info("Could not extract additonal info for current Token");
            }
            return details;
        } catch (Exception e) {
            log.error("Error extracting the information in token with message: {}", e.getMessage());
            Map<String, Object> details = new HashMap<>();
            details.put(DETAILS_USERNAME_MAP_NAME, username);
            return details;
        }
    }

}