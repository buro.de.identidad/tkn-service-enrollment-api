package com.teknei.bid.oauth.config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

/**
 * Configuración del servidor de recursos con los permisos necesarios
 * por cada petición HTTP, además de indicar el identificador de recursos
 * (resource_id) con la que trabajará la petición/autenticación.
 * <p>
 * También incluye las cabeceras necesarias para evitar errores con
 * el filtrado de peticiones CORS.
 *
 * @author marojas
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final Logger log = LoggerFactory.getLogger(ResourceServerConfig.class);
    @Autowired
    private DataSource dataSource;
    //@Autowired
    //private CustomAccessTokenConverter customAccessTokenConverter;


    @Override
    public void configure(HttpSecurity http) throws Exception {
    	//log.info("lblancas: "+this.getClass().getName()+".{configure(HttpSecurity http) }");
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/rest/v3/**").access("#oauth2.hasScope('read') AND hasAnyRole('USER', 'ADMIN')")
                .antMatchers(HttpMethod.POST, "/rest/v3/**").access("#oauth2.hasScope('write') AND hasAnyRole('USER', 'ADMIN')")
                .antMatchers(HttpMethod.PUT, "/rest/v3/**").access("#oauth2.hasScope('write') AND hasAnyRole('USER', 'ADMIN')")
                .antMatchers(HttpMethod.DELETE, "/rest/v3/**").access("#oauth2.hasScope('write') AND hasAnyRole('USER', 'ADMIN')")
                .anyRequest().permitAll()
                .and().headers().addHeaderWriter((request, response) -> {
            response.addHeader("Access-Control-Allow-Origin", "*");
            if (request.getMethod().equals("OPTIONS")) {
                response.setHeader("Access-Control-Allow-Methods", request.getHeader("Access-Control-Request-Method"));
                response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
            }
        });
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    	//log.info("lblancas: "+this.getClass().getName()+".{configure(ResourceServerSecurityConfigurer resources)}");
        TokenStore tokenStore = new JdbcTokenStore(dataSource);
        resources
                .resourceId("bid")
                .tokenStore(tokenStore);
    }



    /*
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/rest/v3/**").access("#oauth2.hasScope('read') ")
                .antMatchers(HttpMethod.POST, "/rest/v3/**").access("#oauth2.hasScope('write') ")
                .antMatchers(HttpMethod.PUT, "/rest/v3/**").access("#oauth2.hasScope('write') ")
                .antMatchers(HttpMethod.DELETE, "/rest/v3/**").access("#oauth2.hasScope('write') ")
                .anyRequest().permitAll()
                .and().headers().addHeaderWriter((request, response) -> {
            response.addHeader("Access-Control-Allow-Origin", "*");
            if (request.getMethod().equals("OPTIONS")) {
                response.setHeader("Access-Control-Allow-Methods", request.getHeader("Access-Control-Request-Method"));
                response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
            }
        });
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices());
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        Resource resource = new ClassPathResource("public.txt");
        String publicKey = null;
        try {
            publicKey = IOUtils.toString(resource.getInputStream());
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
        converter.setVerifierKey(publicKey);
        converter.setAccessTokenConverter(customAccessTokenConverter);
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }


    @Bean
    public CustomAccessTokenConverter customAccessTokenConverter(){
        return new CustomAccessTokenConverter();
    }

    class CustomAccessTokenConverter extends DefaultAccessTokenConverter {

        @Override
        public OAuth2Authentication extractAuthentication(Map<String, ?> claims) {
            OAuth2Authentication authentication
                    = super.extractAuthentication(claims);
            authentication.setDetails(claims);
            return authentication;
        }
    }
    */

}
