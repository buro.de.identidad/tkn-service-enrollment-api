//package com.teknei.bid;
//
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.lang.reflect.Array;
//import java.net.MalformedURLException;
//import java.nio.file.Files;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
//import javax.imageio.ImageIO;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.util.Base64Utils;
//
//import com.itextpdf.text.BadElementException;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.Font;
//import com.itextpdf.text.Image;
//import com.itextpdf.text.Font.FontFamily;
//import com.itextpdf.text.Phrase;
//import com.itextpdf.text.Rectangle;
//import com.itextpdf.text.pdf.BadPdfFormatException;
//import com.itextpdf.text.pdf.ColumnText;
//import com.itextpdf.text.pdf.PdfContentByte;
//import com.itextpdf.text.pdf.PdfImage;
//import com.itextpdf.text.pdf.PdfImportedPage;
//import com.itextpdf.text.pdf.PdfIndirectObject;
//import com.itextpdf.text.pdf.PdfReader;
//import com.itextpdf.text.pdf.PdfStamper;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.teknei.bid.controller.rest.crypto.Decrypt;
//
//import net.sf.jasperreports.engine.JREmptyDataSource;
//import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
//public class TestFideicomizo {
//	
//	
//
//	public static void main(String[] args) {
//		try {
//			new TestFideicomizo().setDataFideicomizo();
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	
//	
//	
//	private void huella(String b64, int pag, int x, int y, PdfStamper stamper) {
//		if (!b64.isEmpty()) {	
//			try {
//			// desenrollando base 64
//			Decrypt dec = new Decrypt();
//			String huellaFinal = dec.decrypt(b64);
//			byte[] imageByte = Base64Utils.decodeFromString(huellaFinal);
//			File rutaHuella = new File("/home/fingerToFirma.png");
//			if (rutaHuella.exists()) {
//				rutaHuella.delete();
//				rutaHuella = new File("/home/fingerToFirma.png");
//			}
//			InputStream in = new ByteArrayInputStream(imageByte);
//			BufferedImage bImageFromConvert = ImageIO.read(in);
//			ImageIO.write(bImageFromConvert, "png", rutaHuella);
//			
//			// estampando en doc
//			Image image = Image.getInstance("/home/fingerToFirma.png");
//			PdfImage stream = new PdfImage(image, "", null);
//			PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
//			image.setDirectReference(ref.getIndirectReference());
//			image.scaleAbsolute(71F, 71f);
//			image.setAbsolutePosition(x, y);
//			PdfContentByte add_watermark4 = stamper.getOverContent(pag);
//			add_watermark4.addImage(image);
//			bImageFromConvert = ImageIO.read(in);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (BadElementException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (BadPdfFormatException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	
//	
//	public void setDataFideicomizo() throws DocumentException {
//		try {
////					Pagina 1 
//			String num = "666";
//			String nombreTitulo = "Alex Josue Garcia Dominguez";
//			String fechaTitulo = "27          mar         2019.";
////			Pagina 2
//			// BLOQUE 1
//			String nombres = "Alex Josue";
//			String aPat = "Garcia";
//			String aMat = "Dominguez";
//			String prof = "Ingeniero";
//			String fNac = "17/04/87";
//			String nation = "Mexicana";
//			String residencia = "N";
//			String curp = "XGG2YU2KK91SHX2Y";
//			String eCivil = "S";
//			String matReg = "2";
//			// BLOQUE 2
//			String calle = "Calle 1519, #123B";
//			String colonia = "San Juan de Aragon";
//			String delegacion = "Gustavo A. Madero";
//			String edo = "CDMX";
//			String cp = "07918";
//			String pais = "Mexico";
//			String eMail = "agarciad0705@gmail.com";
//			String telCasa = "      52         55        5521583817";
//			String telOficina = "      52         55        5521583817";
//			String rendicionCuentas = "1";
//			//BLOQUE 4
//			String b4 = "c";
//			//BLOQUE 5
//			String poderCobroo = "Elenita";
//			//BLOQUE 6 depositario datos 
//			String depNom = "nombre1 nombre 2";
//			String depProf = "Ingeniero";
//			String depFeNac = "17/04/87";
//			String depNaci = "Mexicana";
//			String depAPaterno = "apellido paterno";
//			String depAMaterno = "apellido materno";
////			Pagina 3 
//			String ciudad = "Ciudad de Mexico";
//			String fechaP3 = "27             mar             2019.";
//			String fideicomitante = "Fideicomitante";
//			String fideicomitantePor = "FideicomitantePor";
//			String fideicomitanteCargo = "fideicomitanteCargo";
//			String fiduciario = "fiduciario";
//			String fiduciarioPor = "fiduciarioPor";
//			String fiduciarioCargo = "fiduciarioCargo";
//			String conyuge = "Conyuge";
//			String conyugePor = "Conyuge";
//			String conyugeCargo = "Conyuge";
//			
//			
//			int fontSizeTitle = 12;
//			int fontSizeData = 7;
//			String SELECT_MARCK = "X";
//
//			String DEST = "docs/ContraFideicomisoP1temp.pdf";
//			
//			File file = new File(DEST);
//			file.getParentFile().mkdirs();
//			
//			PdfReader reader = new PdfReader("docs/ContraFideicomisoP1.pdf");
//			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DEST));
//			// ************** Pagina 1 **************
//			PdfContentByte pag1 = stamper.getOverContent(1);
//			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
//					new Phrase(num, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 315, 660, 0); // num
//			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
//					new Phrase(nombreTitulo, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 305, 520, 0); // nombre
//			ColumnText.showTextAligned(pag1, com.itextpdf.text.Element.ALIGN_CENTER,
//					new Phrase(fechaTitulo, new Font(FontFamily.TIMES_ROMAN, fontSizeTitle)), 350, 207, 0); // fecha
//			
//			//************** Pagina 2 **************
//			PdfContentByte pag2 = stamper.getOverContent(2);
//			// BLOQUE 1 GENERALES DEL Fideicomitente
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(nombres, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 680, 0); // nombres
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(aPat, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 670, 0); // apellido paterno 
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(aMat, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 670, 0); // apellido materno  
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(prof, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 650, 0); // profecion
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fNac, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 650, 0); // fecha Nacimiento
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(nation, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 630, 0); // nacionalidad 
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), (residencia.equals("E") ? 455 : 404), 630, 0); //recidencia
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(curp, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 600, 0); // Curp
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), (eCivil.equals("C") ? 453 : 407), 600, 0); // estado civil
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), (matReg.equals("2") ? 442 : 348), 580, 0); // Regimen Matrimonial
//			// BLOQUE 2 DOMICILIO FÍSICO QUE SE ESPECÍFICA PARA OÍR Y RECIBIR
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(calle, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 507, 0); // calle
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(colonia, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 498, 0); // colonia
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(delegacion, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 498, 0); // delegacion
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(edo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 488, 0); // estado 			
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(cp, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 488, 0); // cp 
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(pais, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 476, 0); // pais
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(eMail, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 476, 0); // email
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(telCasa, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 458, 0); // Telefono de Casa			
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(telOficina, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 458, 0); // Telefono oficina 
//			// BLOQUE 3 RENDICIÓN DE CUENTAS.
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 202, (rendicionCuentas.equals("1") ?  405: 385), 0); // En mi domicilio particular 1.
//			
//			// BLOQUE 4 NTERDICCIÓN.
//			int y = 0;
//			if (b4.equals("a")) { 
//				y =  332;
//			} else if (b4.equals("b")) { 
//				y = 322;
//			} else {
//				y = 312;
//			}			
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 73, y, 0); // INTERDICCIÓN.
//			// BLOQUE 5 PODERES PARA COBRO DE SEGURO
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(poderCobroo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 364, 269, 0); // Nombre Apoderado
//			// BLOQUE 6 Depositario
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depNom, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 207, 0); // nombre Depositario
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depAPaterno, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 196, 0); // paterno
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depAPaterno, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 196, 0); // materno 
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depProf, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 176, 0); // Profesión
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depFeNac, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 400, 176, 0); // Fnacimiento
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(depNaci, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 156, 0); // Nacionalidad
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 405, 157, 0); // Residencia. a 405  b 455
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase("CURPCURPCURP", new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 150, 125, 0); // Curp
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 407, 127, 0); // Estado civil soltero 407 casado 453 X
//			ColumnText.showTextAligned(pag2, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 349, 107, 0); // regimen a 349 b 443
//			//************** Pagina 3 **************
//			PdfContentByte pag3 = stamper.getOverContent(3);
//			// Documentos Anexosv DEL FIDEYCOMITENTE
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 692, 0); //e
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 682, 0); //m
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 671, 0); //n
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 661, 0); //o
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 650, 0); //p
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 639, 0); //
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 629, 0); //otros
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 618, 0); //otros
//			//--------------------CONYUGAL
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 692, 0); //e
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 682, 0); //m
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 671, 0); //n
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 661, 0); //o
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 650, 0); //p
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 639, 0); //
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 629, 0); //otros
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(SELECT_MARCK, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 546, 618, 0); //otros
//			//datos
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(ciudad, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 190, 598, 0); // fecha
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fechaP3, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 273, 598, 0); // fecha
////			Firmas
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, 7)), 63, 550, 0); //Fideicomitante
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fiduciario, new Font(FontFamily.TIMES_ROMAN, 7)), 228, 550, 0); //Fiduciario
//			ColumnText.showTextAligned(pag3, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(conyuge, new Font(FontFamily.TIMES_ROMAN, 7)), 405, 550, 0); //conyuge
//			
//			huella(b64, 3, 63, 550, stamper);
//			
//			
//			//************** Pagina 16 **************
//			PdfContentByte pag16 = stamper.getOverContent(16);
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, 7)), 106, 474, 0); //fideicomitante
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitantePor, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 462, 0); //por
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitanteCargo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 454, 0); //cargo
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fiduciario, new Font(FontFamily.TIMES_ROMAN, 7)), 366, 474, 0); //fiduciario
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 346, 462, 0); //por
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitante, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 346, 454, 0); //cargo
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(conyuge, new Font(FontFamily.TIMES_ROMAN, 7)), 106, 399, 0); //conyuge
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(conyugePor, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 387, 0); //por
//			ColumnText.showTextAligned(pag16, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(conyugeCargo, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 80, 379, 0); //cargo
//			// FIN PAGINA 16 			
//			stamper.close();
//			reader.close();
//			System.out.println("COMPLETADO Parte 1");
//
//			generatep3();
//			// - JASPER ---->>>>
////			String JasperPdfTemp = "src/main/resources/jasper/FideicomizoTemp.pdf";
////			String jasperPath = "src/main/resources/jasper/report1.jrxml";
////			JasperReport jasperReport;
////			JasperPrint jasperPrint;
////			Map<String, Object> parameters = new HashMap<String, Object>();
////			parameters.put("num", "555");
////			parameters.put("nom", "Alex Urrutia");
////			try {
////				jasperReport = JasperCompileManager.compileReport(jasperPath);
////				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
////				JasperExportManager.exportReportToPdfFile(jasperPrint, JasperPdfTemp);
////			} catch (JRException e) {
////				e.printStackTrace();
////			}
//			// - FIN DE CREACION PARTE <<---
//
//			// INICiO PARTE FINAL --->
//			String tipoValor1 = "Chekes";
//			String porcentaje = "100";
//			
//			String numDias = "15";
//			String fideicomitente = "Maria De La Luz Gutierres Gutierrez";
//			String conyugeP2 = "Maria De La Luz Gutierres Gutierrez";
//			String fiduciarioP2 = "Maria De La Luz Gutierres Gutierrez";
//			String interFina  = "Maria De La Luz Gutierres Gutierrez";	
//			String DESTP2 = "docs/ContraFideicomisoP2temp.pdf";
//			File fileP2 = new File(DESTP2);
//			fileP2.getParentFile().mkdirs();
//			PdfReader readerP2 = new PdfReader("docs/ContraFideicomisoP2.pdf");
//			PdfStamper stamperP2 = new PdfStamper(readerP2, new FileOutputStream(DESTP2));
//			PdfContentByte p1f = stamperP2.getOverContent(1);
//			//tabla tipo valor
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 462, 0); //Tipo Valor1
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 462, 0); //Tipo Valor1
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 451, 0); //Tipo Valor2
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 451, 0); //Tipo Valor2
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 440, 0); //Tipo Valor3
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 440, 0); //Tipo Valor3
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 429, 0); //Tipo Valor4
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 429, 0); //Tipo Valor4
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(tipoValor1, new Font(FontFamily.TIMES_ROMAN, 8)), 250, 418, 0); //Tipo Valor4
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(porcentaje, new Font(FontFamily.TIMES_ROMAN, 8)), 398, 418, 0); //Tipo Valor4
////			Plazo
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(numDias, new Font(FontFamily.TIMES_ROMAN, 8)), 323, 336, 0); //Tipo Valor4
////			Firmas
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fideicomitente, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 60, 130, 0); //fideicomitente//			
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(conyugeP2, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 180, 130, 0); //fideicomitente
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(fiduciarioP2, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 300, 130, 0); //fideicomitente
//			ColumnText.showTextAligned(p1f, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase(interFina, new Font(FontFamily.TIMES_ROMAN, fontSizeData)), 420, 130, 0); //fideicomitente			
//			//fin parte 2
//			stamperP2.close();
//			readerP2.close();
//			System.out.println("COMPLETADO Parte final ");
//		
//			try {
//				String MERGED = "docs/ContraFideicomisoFinal.pdf";
//				File mFile = new File(MERGED);
//				mFile.getParentFile().mkdirs();
//				List<InputStream> pdfs = new ArrayList<InputStream>();
//				pdfs.add(new FileInputStream(DEST));
//				pdfs.add(new FileInputStream(PART17TEMP));
////				pdfs.add(new FileInputStream(JasperPdfTemp));
//				pdfs.add(new FileInputStream(DESTP2));
//				OutputStream output = new FileOutputStream(mFile);
//				concatPDFs(pdfs, output, true);
//				
//				System.out.println(getByte(mFile));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			System.out.println("COMPLETADO UNION");					
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static String getByte(File file) throws FileNotFoundException, IOException {
//
//		byte[] savebyte = null;
//		try {
//			savebyte = Files.readAllBytes(file.toPath());
//		} catch (Exception e) {
//			e.fillInStackTrace();
//		}
//		return Base64Utils.encodeToString(savebyte);
//	}
//	
//	
//	private final String PART17 = "docs/ContraFideicomiso17.pdf";
//	private final String PART17TEMP = "docs/ContraFideicomiso17temp.pdf";
//
//	private void generatep3() throws IOException, DocumentException {
//		// Fideicomisarios Segundo Lugar 1
////		List<FideicomisariosDTO> fideicomisarios = new ArrayList<FideicomisariosDTO>();
////		fideicomisarios.get(0).getNombre();
//		// Fideicomisarios SUbstitutos n
//
//		File fileP2 = new File(PART17TEMP);
//		fileP2.getParentFile().mkdirs();
//		PdfReader readerP2 = new PdfReader(PART17);
//		PdfStamper stamperP2 = new PdfStamper(readerP2, new FileOutputStream(PART17TEMP));
//		PdfContentByte p1 = stamperP2.getOverContent(1);
//
//		// TABLA 1
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Josue Garcia", new Font(FontFamily.TIMES_ROMAN, 6)), 45, 622, 0); // nombre
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622, 0); // edad
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Hermano", new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622, 0); // parentesco
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("30", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623, 0); // Porcentaje
//		llenaTabla(p1, 622);
//		// TABLA 2
//		int yplus = -103;
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Josue Garcia", new Font(FontFamily.TIMES_ROMAN, 6)), 45, 622 + yplus, 0); // nombre
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Hermano", new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622 + yplus, 0); // parentesco
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("30", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623 + yplus, 0); // Porcentaje
//		llenaTabla(p1, 622 + yplus);
//		// TABLA 3
//		yplus = yplus - 104;
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Josue Garcia", new Font(FontFamily.TIMES_ROMAN, 6)), 45, 622 + yplus, 0); // nombre
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Hermano", new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622 + yplus, 0); // parentesco
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("30", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623 + yplus, 0); // Porcentaje
//		llenaTabla(p1, 622 + yplus);
//
//		// TABLA 4
//		yplus = yplus - 104;
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Josue Garcia", new Font(FontFamily.TIMES_ROMAN, 6)), 45, 622 + yplus, 0); // nombre
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178, 622 + yplus, 0); // edad
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("Hermano", new Font(FontFamily.TIMES_ROMAN, 6)), 218, 622 + yplus, 0); // parentesco
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("30", new Font(FontFamily.TIMES_ROMAN, 6)), 272, 623 + yplus, 0); // Porcentaje
//		llenaTabla(p1, 622 + yplus);
//
////		opciones 
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 236, 0); // a
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 228, 0); // b
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 212, 0); // i
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 204, 0); // ii
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 196, 0); // iii
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 46, 187, 0); // c
//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 171, 0); // i
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 163, 0); // ii
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("X", new Font(FontFamily.TIMES_ROMAN, 6)), 74, 156, 0); // iii
//
//// firmas
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("fideicomitente", new Font(FontFamily.TIMES_ROMAN, 7)), 50, 50, 0); // fideicomitente//
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("conyugeP2", new Font(FontFamily.TIMES_ROMAN, 7)), 175, 50, 0); // fideicomitente
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("fiduciarioP2", new Font(FontFamily.TIMES_ROMAN, 7)), 320, 50, 0); // fideicomitente
//		ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//				new Phrase("interFina", new Font(FontFamily.TIMES_ROMAN, 7)), 480, 50, 0); // fideicomitente
//
//		stamperP2.close();
//		readerP2.close();
//		System.out.println("COMPLETADO Parte final ");
//	}
//
//	private void llenaTabla(PdfContentByte p1, int yinit) {
//		int yminus = 0;
//		int count = 0;
//		while (count <= 5) {
////			622
//			ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase("Josue Garcia", new Font(FontFamily.TIMES_ROMAN, 6)), 45 + 270, yinit - yminus, 0); // nombre
//			ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase("33 años", new Font(FontFamily.TIMES_ROMAN, 6)), 178 + 270, yinit - yminus, 0); // edad
//			ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase("Hermano", new Font(FontFamily.TIMES_ROMAN, 6)), 218 + 270, yinit - yminus, 0); // parentesco
//			ColumnText.showTextAligned(p1, com.itextpdf.text.Element.ALIGN_LEFT,
//					new Phrase("30", new Font(FontFamily.TIMES_ROMAN, 6)), 272 + 270, yinit - yminus, 0); // Porcentaje
//
//			if (count >= 3)
//				yminus = yminus + 9;
//			else
//				yminus = yminus + 8;
//
//			count++;
//		}
//	}
//
//	public void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate) {
//
//		Document document = new Document();
//		try {
//			List<InputStream> pdfs = streamOfPDFFiles;
//			List<PdfReader> readers = new ArrayList<PdfReader>();
//			int totalPages = 0;
//			Iterator<InputStream> iteratorPDFs = pdfs.iterator();
//
//			while (iteratorPDFs.hasNext()) {
//				InputStream pdf = iteratorPDFs.next();
//				PdfReader pdfReader = new PdfReader(pdf);
//				readers.add(pdfReader);
//				totalPages += pdfReader.getNumberOfPages();
//			}
//
//			PdfWriter writer = PdfWriter.getInstance(document, outputStream);
//
//			document.open();
//			PdfContentByte cb = writer.getDirectContent();
//
//			PdfImportedPage page;
//			int currentPageNumber = 0;
//			int pageOfCurrentReaderPDF = 0;
//			Iterator<PdfReader> iteratorPDFReader = readers.iterator();
//
//			while (iteratorPDFReader.hasNext()) {
//				PdfReader pdfReader = iteratorPDFReader.next();
//
//				while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages()) {
//
//					Rectangle rectangle = pdfReader.getPageSizeWithRotation(1);
//					document.setPageSize(rectangle);
//					document.newPage();
//
//					pageOfCurrentReaderPDF++;
//					currentPageNumber++;
//					page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
//					switch (rectangle.getRotation()) {
//					case 0:
//						cb.addTemplate(page, 1f, 0, 0, 1f, 0, 0);
//						break;
//					case 90:
//						cb.addTemplate(page, 0, -1f, 1f, 0, 0, pdfReader.getPageSizeWithRotation(1).getHeight());
//						break;
//					case 180:
//						cb.addTemplate(page, -1f, 0, 0, -1f, 0, 0);
//						break;
//					case 270:
//						cb.addTemplate(page, 0, 1.0F, -1.0F, 0, pdfReader.getPageSizeWithRotation(1).getWidth(), 0);
//						break;
//					default:
//						break;
//					}
//					if (paginate) {
//						cb.beginText();
//						cb.getPdfDocument().getPageSize();
//						cb.endText();
//					}
//				}
//				pageOfCurrentReaderPDF = 0;
//			}
//			outputStream.flush();
//			document.close();
//			outputStream.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			if (document.isOpen())
//				document.close();
//			try {
//				if (outputStream != null)
//					outputStream.close();
//			} catch (IOException ioe) {
//				ioe.printStackTrace();
//			}
//		}
//	}
//	
//	
//	private String b64 = "gdE+Fua2A9PSQeiLFsbnq0KfIvfTZMtZwCdKlhCmEtbDZisP4+8vnwcwN1gzY8jBy9HRPvm1qaaUKnz2Z46WcdbEsfDApymJLMSi/BGb+9jzOVjOIyxMhgFm81FSj3fKG3boSSC9KZEu6heZ5YjBCmX9kj5FlNaVDIPZhvSEo27blP8H2kOGOy8iHYqIpZ5Yqn1XbEtibiE8ZlOw377MinzMPc7WuDmh0bxsCgYcSFHHWO9DoJsM5+Hf8DpJsc1nb4Czb7pR+UkAs55uuBVxrcdlE9kC2PFFJxetWbXTisZVLb3fP5nAACPlbF1Tk4dJtvZsXbNxYPD3Szp5nSQGHq3tpdLC8QLHLL3b3YcuQ+fpzzfcdYbLq7kt91VduQVvksyID7F4SP81DcwZyY7SMeeGdT9ayDe7NsXai9Szi7e91qfKKHYohsMgExNH/ghM5aYwDx4mp+rvMsbRSi7gcAU0299O7pXtrTaQHpOwhGe7WMjTz5KCuCvyH2Kda04945+2RoC2pehm38SKbIJnm3dG6Hjqz6VuUJK9fNn0aRP/7b9gwFVA9yyN0YrnFco+wWmLFAUfT9xy4CBvGDzl8td61IoksKIKSANZeWI4bGtfW7EFG9ZY14TL1fxSiDYDEYShFmmAkw8f1hzbR+xD8YQN8pX2kxF6OT5tw3+/jpewQIG7MUaonjgVX89JwOu0n5irp5SO8z1AWiAZY7QMrmr5+6bLZIjTq7WdduOkCQ+olLoeMgo5tdeWRiMPLbdFEQuiWx0v0kSKWcz1zKC7PWL00Sw/aWgYL+mmuhehfuUnVLAgfmCABZcwIyezasq/Lw+8JZC3kA+Gk+FtC4j59mcRDQ/WUWIxcOd8KUPfUMJk7H1RGyYrMmpqlIeOzBQrP1UI85pLqer74mB2cQ16euC0vqnP22w/KZZLJib/zbqdEp8JTBr01XJ0gqmhaC8LHeGtxunS8761s/dPlha1tMM4YokTbIpS3682zI9zePSQeOJjEsFlvbAYqQ0pbmTzj3SiDLrIaFAU5Ddv/V/BRQ12L+5q1sEmE0zW8QHT5Sp6d1n8Tc8T6PqwNmjxBaXl9KTgqhi3aNiHV+GQEtfl/lJ6uwecqUokoZ6y3LbW0+ppO9bS0NQyYIYbHHVbIsttcCxRhuucaEHoONl7bLF7L4mG3CMlHmnhVxmbQ9PFOBj3jZwB6lArIv3UbeJ4lE9wshnTx08DINawh6zhlva6MgeAPKhhl+nrry2k4d9pokPiub1vp9CRtDS3spjLd020HfLW9zSCSQLuQWoz1ivJ8BYzy0YrTKliUMwNnRft4pHcyHvDpxo478LkPi1n3x5IwEqg/wL5m3ltipsM/Uhp+w5BbTFro3Xj1515NQH9HscP8zS7X49reIU0Rj7Dpxo1s3O7F2jrp/18i24vTimkwIw/YF4nDyPTOgt9S6i4R41wFz0w4IvmXbmBvTDq/XYig7Ur2ViXQ/S5Uswr4pxjDj22mXLgBpo6tgyrkXyWcaZJkukj9lrPfxLw8ujDLoX3Hca8kx7+HX1GHHTLvcp9ITWNjgcKpWYAvrc8Yt8dTQdJ3jrGu4ckHocqTIF5ywkqyFllN+E6I1y1foiPVP94z2qNI+4EhL6tU96lRMAx0jcCPQm9YWM7PH5XYLDhiBFv0QNBYrGevaoEop+uTUoJMFXQ7tkIOtqGwBvhimlykU3c35fmJXPgYx+gzuZ23G5YRwvK6UFIj00kIeAHGwdCAfQd81YBX6wN4QZ6GhgUNMFt69hGWag96PBrMf4WNf++XjWyNcGovocQ71LU0uKeTurcRrQWP72cpzL7mhY1ARAYgLcn0BzlBogWE2AX9DVThUoIJ3RTYWtjpp5SnMr5Kg1Y1PLRStsxd/U808P1AXxBBmnMEpdUg2ylvJCCGDS2/hPUwphsI1i3Egasw+naG3lSJzStIDA47mllqitBE2qmuUKDbMuO1t1rmr4NUIM0C7AhYkTerOwFs5qMwsVfWjFTPNADiZ/5vA1oNMVh48rAfXJZ2QzhbxDAaRZkGjkQnQYIaXTIln5CHmU2JWG08YOMsTwj1DIOe2RUFJjk37o1ZT0Pw0QzA2LjZcfjNUztKmAMgSwzV53wKs+iHD+T+B+uBphfzinQ8BGB15wTaJCkXcEYIqA3412nAZoB7aA23QiwZxF4dzSx7FQ2VwffMnCbpBmlUMQSocCSy6latmqpt8ieooAiizYe56X980YiWstBO0iv/k98UCGiZkSVVn9O4k/+G6uBNVqbIG4a7CLvMK1kZSq+Lv3YBti9yPQlI5+3beRSBFgA1FTLzaosLlOoHhlowsB+hphylzEmPh2STtiR/roKNaBvEMsgsatVHlQhVT2EmmBBeSGMp504M7e3z6nZ+nT+CB1EB9uircO3IuYxRXNSQ1BTOqGx/jNLVodOZoWMnRQVTBTzU4oajiDJnQAWM9iS6fDoChmGXWolvg8Hy4b2gpLmRLvgpkOAcViQThVnRGQRteaUHcu+d0V/EZtbY+rXLsJa7GBIKo+AKSimoifJ3Ii4dMBxfDBenAzJLhY7jM1tbSHHHOrlstOaCQI+Xhoom1hxiKdRUj19bi9K/EdmF31EgTBya7nnlreiGVP7ac8dkmG/fUSfABinUmH+xYPweZVywB/yHPqI239BzepmWIyTLPWhk2/jvKrsvIkt/K2jU6W84vNDSr0CkOnVA9siyM0hs83inQ+u6WYMtklcT30SdszIJRkLI9pw55vUZDRnXpOcioUTc7gwweEakYea6wT4WrzrZyfvEgGKRyL143nqSSgGVjVfOgGGVX6lUhHUTZPz50o8Kf+TgITcP0Jlis+P+QqUoEkMqgDp2TwLsfTXFacrd4jSWwdFLY7MW9HGcYERKRg1oAP3eeZvy+NXAf5fnlPEUNjgFLJwC2wQ+NUTh1CxptVqeBTFS10lqCYKlXlpChuXOa5XTsgJGvJ67rlRzFrMI0QVZZGUaGyfeetO86uTrWzqzHOk2jLRJJNAdmlK46tdr6sZvDzrFBU7gQIsngQdWJFqmkMRUNLApOjR+Z84estQAtYd0i5Z/GJcUTmIQDJzPMHEbGez0xJ6kTiHeqC77arIW5XMA0HCxKKuDk/MBoUBcWDouVx1v0Xd/5gabWKdZwvoy5dASi0FSsK4truWt9uIF7hbFqgN+19Kwi8uzZt9eMAxQg8GpD5OPAEcJGwsSmik3lbZxs0N85/0y6KO2wAXgdNlv/F4vUL8lNvFp3bIHsfqF3wEAlLc1wD+Z4UjR/FUcJb+XJYWpM5To3/FHjo0S7COGkopsf/2xVqVGdLzop3blHnxmcsTyQLyZn07t27Q80qtlnSdl8YtHNkj63VOE7OM4BKsq0bkSS4OIBrqAoq6FQQ3k005OIZDxgvi6js4qinT2Pw1n5je2hA9wsKkj7BCsosz/se2g7PHtx/Jnnu89L9aMxpQ+5jeuDbxU9ng1i0PCLxSF43aj2JyGEVx5jbO4LOCItVS1iwbSmKD9FtzJNU1toEoc1/248+binpWWerPleDex9C2b/cWoo0i5KVPfhQhmKMDSkUWviEuM/PYLg8CudRy/Wh2GV09pScRflldtlLxFZZyefrfsAfbD4j1y72MimocYyPh2Q5vodTnKdpZbcl6PZUnVv/ECGgHMCvT7fCZhr6ke4m3eDWevLBrMSeFSfWAxsdD7JV0cl0SHVWYI7ZM9DmF16fnxpUy2xIWzkFlDl8kbj009ssrMhKrI2qpbuIiKL79tFLn2tyr/S1ddWVxzkxB4S7/ruyPGzUCYmiSoBYM3ChhF6gNe6mRhb9fWrpkrW4ubD1BVvNOLkbe2ZSt/qUDRjehW4T07ClTVraBwBj1KpLatx8j75hzPDzCT9k0a+mDtGJMEDFBN3x5R3nl3nipUIIGKhNq0EUh6KodHzDFANNw9TtwJLWiaWCA0Uyue6Vg5Ep9M9JuKOWNYQlAc5Tbww+Nu+YargEgpgjYqRCCOTw9UoKwUcUhvuQ8InItEf2+VbdfFVv9n9yPH4cCkyCIWCeB0Kcb5qubqYOG6xdhJ22oNGnd1x20LPy0Wv6qreMU907vEO11HYZzJ5sf2YeUhluPbdnLK7BlCpP8cHm8W2GG+Q5Q5rqTpbYmvgwwgia6V2y6jQOe/eQtunDrY2vNr0IHDyN6Ne0xghDeNY0hbBizd1b5DLGKMJxb/BYjGmQHKix3TYUlC0rsEYvj2GyDWUck0VN2E0yflzbVadOrg8YKNlaG2mAr2JxOIOGn05SQIopi4HNu4N8RyGp8RS2l5z9Fk8pAopNky/KBuNZhf8+pgZfpeAGgiTHGNZAbz14n9zVEPOud+0fInEIbc07KUqce5/fNyL0zyT8Vj6EZlqUGKZAmyrOOc2fM9jwjoCv8TEZR3D4JRbm5jwR8Yl6UzQC/EgKJnLx1sO/61yrsKZv8JCp42zEGBFS4eKB/hDRKdbKrQofmZYdCZUCQqk122HZOo/ZsrIimsMxrNBbs4UaCtGlpJtwnF/LUYBf+nTzZiG/Rye6pE/bZhHn0/XwbiuzmhCIghO2B/hNmpaYupQpiAdwj4h88L8JLVkvVI84VBQS6lEyCZF3pshAvdczj4RuxwAnF4DLTQhfFR0ohH3Ab/0eG9or3r3cYaFPcuRbXV6UkSz9Bct5v2DAnanMSbEj7DQ6N4EM9k0VaFDLauN7IivKxHLqjK026A7S73mgHV78VyOJ0X9/Yku4BwBxU4cyLgq0o3aYKBpGyzWH+s36A3kccOU55a3/BSZZ2j0uBUrA3mS/wKA3JhXEzim0cL++ppTEFnUtgHzKc009hCePePhTYTuVuHA6znryr+mT7Xz11UcRs1RILjddD6+epKjzONw/0rhysaPNWr0gMgiWM4MS7bgorNpUTTPLnJU4CRIOk+2zDVMFD/DRZC0h7VVk4490qr2SnuLhtFFvB+xlc/ii1aIjLfFiKjyJAJ+WGEL/BPYj+s0/24uJiHDYOah+3kJNMhfLeftyOo11G0W4u3lpWUWooHDkoyNfpyZoP2TqYeMnTJYNLFOHbYgLrkYD0ylUC8STBpBC9EmbnfMbeJcuL6fXHsUtveXxkNm7vHNT2dQuLR7K3h8CHqR59ZikmYsvZ1iLG3BdL62yGB2hF7igH4Tnv4xV90sFuGpxsW3mQXQ11wNBx3cut/9cjE0Jhc1cFuDZ8NvEtAUDfNNE57f+ziMIqxW2v7gllVgOrdfBwZ06dHWqOCShbehqnSHuOZB8pVhVQKqSWJG0uSNX67MUJbxjkNuyDwygXGJUWrujPMNKWrG0gGMJs2TU6Y6fDK391a0i7M/21R3b6+nlIglmh/S+9ugzxgc0OZj5LU9WF4zxvu+sMEdtx0s7ubdXjdqKsupN5m/uW5ulPCrEr22hUdoauulgmMs8NKLIhcmV7548nlG4G/MK1slU2KBYwe4p7b53qJwXL6btXg7BSw/0bDoTTJRqGwQRRzjtum21JVI7pc6rc4YHsnTTh1qqzyx6WYOZSTJw4W3NBbvxhJsfmU9xdo8DjPGcL579ZXLfqDHQPUKcj0KMvZTBxuhvEH5DHvBKkd+6Hj+AfJBkd/9IcwN2e7RQUrYvJJC3IRanjNU4gR93xwXFWvRWWhO+A/bQFc6ghTfE/zXDIvqyCPXKS8pkcInb/zOI0gT1eUwsXUTnCnvc7sq7PbxnkWoj/C4MZOjTacpHMhbTR9EUQHNOi+NONaYrVXwMfxF3GjcxQRRDhC2bdwZcKt+cVVamOaBXFl2CBWW1c0nJkdy+zDz/97I35F8+/sxMKeanKrxL1XSw2+aLWE6TYP1+/fxankpXTvEYC3xgcT+LX+qDZ7+u8UT3cN2JVVx85r5pJhBaCu0GuLYrnilj7Gg1/3NnirrxwlqDDoj/IZSu54d7KxDJyudzw0r5i7VVlDKkaoFCrl3v+k7WgR7T5jzfeKVNZElfJn0u0HRQmSXWBs6CmLvv2kGKsyA6WinvkmMf0MlGiJneRKnhZ9PKSdJamCGFRtfI5x5MMisKmvwRBwN8QZSs9BQfvp0LAQO/w7FhSQ/bluswMt/XQKB12MInZgSmlGXjLiZUvIojnGGF5UEmrM/bPiODp224sPgqbBAJkBRO4cjtRQGbs6K8HIavuObeHcwLzm+iKbEyLPJs9zNUjAknvHdzpG3GJqjdRtwRLq/0uqDAHoFqnenc7Coe3ZTIDNexMqbSqcDcrJ4pdAIPi/IMbLmt81nbx9ybnbLFaeH5Cip7b2q+79iYOXQ2ZNTyZTMIF7pCEOq9JkIjnMBTmvLxDmpLZPdBvU3Sm14v+pqpyYgGMEjr9W4r/vmydQaddNDvp/tndL7hJkdt4179NGX1+hMZDE8ertmkcizqysvFwCaDxGbSdt2fj9CpzqM91vt7mjr7EANqJVCcYklAqqMiszWChaxeMeOtOgIB61rkWegFSw4lKa78lJUZC2aVbqU1DutSAFhkWwxenf56sBsKjPK17Q2mHY9jXTj9n0olbiSwQ+SCpGXNWxh7L5/uI2q7hLAE+P6ALNQ5HBqj8OvHIr2feSDxw98J1VlsQR36etU8XgNpzjvh1fyEcSpHfe6Nuh7ql2SLFfWH8jvHR/tS9Ns7daOZtbieIuwOxlV7X4L6wuv8hIvTJU/3GQ5uSj8vvyBjYYlVmoTaPHlKf13nVC4LMVMVZ4Ub4WAKinj7IT4UsBSsegV5VA9J5JJkWN3t87qFi/bR0Ugj4TJU33EHAufUoMFatgnKOR6t6i/B+4XCc/Qlsct2NvLnCsFsriFHuFiz0CDk+V+oz0hrusM1fZQfCFabuBhy5GfptW18/bUAeecOR9d4VgSG4JRiqk7CzP6E6kRtRNG7G/FuHfG7Ubkde5XRumI09MbHPixKDk1X5AHj774CC0muqaoWyQpgsmSNuWJMbAZS5asJXb+pozLFbKmM5/Aik7E8TlhkcVFF5P0VfaSH0A10r+CTWhKny43QqGBm3cmZQxuaXc6YL7riF/h2VG0Sok5D91lFMlIZGms6xSa3xpR2oT4DttyPqW4JxY1bne/u+pcv6pB2p8q7ede68aTpQ1IpqzSiIVvojfMrNG+0m/iH6z/1XqchTLnLn1u5EBlbEhAjV+Q4VLx4IF/b8/ob1fmYECO/62JtQslLL1dDZ45NzfpoXxvVcSjD2ySKNZGfA9VtrZ3r8nlB88THZONrSW3T1+7Je2mqNQjkvjHfVG8VT4dmgeNq7wfPyb1de7tUy72R8Zsotee6b6YyZKbAfHfiFZwz+0vEBHeFagCO4SAvxcMT1vJ6GlMpqkIFu81x0DQDFLcBqX2bu7YWePfMXrYwyTV7zXJQEQFMwF2aSFVXWsQbP7g3JptrwA5kxhucoZUhLeCJF2e/wWE53ii/M4S4tbcpNr1yWzYvVLoXFs22Fmca6gi1BAaZxx5RtdZxmE3euZylcc53tm2gRqivxy3iGWXMjR/K0DiIsIrxx34+TVvXQ4GWEFdWJ8lsq8+q8QDXLvPh6/n6pjBTsdrhQkvF51pZkiGmNzrRR30AqNWQK363CiqYw7IM4304o9yYhmAziadcGdygeYJ+4x6Ir34CxWuZE/o+O1Joa+o9+6sNp6K9lpEFse22MFojF78FWeWZmZuTkamAaY5zl7E0DIU+nThroC/nnIvL4V4z9yyeQqgDLnMBhWJY85hpuxXRcuXtqtQ3t3XvWrII+S6q14p67pQeJ8clCSOr2o4GxIAr3VkhSVh8t3lCmj1jFcZb+jc7sHfYCfUlOIiIxuXB8kt/l28witmiQAEkMnHptlsKnWJDzKDAd9RhA7inf9Av74noyQs+Yk/bJe0DAFFK/tQPJNcISkfZbT7/GnbZTsCs932O/YysYr2j69MnfFWOH2ncIuKhbmnB9JGTlnLu/nr9U/3Iq0mLOj2zhlXoRtOlftxY+hzO3xF0/4q9zHr5GKUHr9eksG5sXhUsW+gjj+Wil9dV655lfLnViYvwUAWEbGPpl+698EolY8qn/2dRlQPIAxCPFvm5C0KD9MXbOGzwbnHFP1QpO2KgXtIZN8RrJbqnpc7EfGez4qXCf5DyZjxWS9nbfbi2ILm6J4eaXxnKvzTwF0UDdGcNNUpDiADhZGiwKwwxCPRz/jS2Qw+EoCO0tM52wcEyNiN0YiBsLFjxvnP5PLG4rnoTXYS8m5ACCPE2KAOUn3MfN7XT3Hrihzhlz9ucirknKYsottS5TRZa6mmrmTXB98SEDp67asmevHaBwycwIa6Jtn2KRvg/Y4GnnqYgn8TXuszInstB8nWqtCEy/n5GAkSxaklMEJy85cPoV8jrXflmyVQXqDdnPLI8xbzhX2mo74rUx2bt7xifJ/1yj8QMzmfrMTQh9x5TCjw9M3WFGBQvOZSVPh8HxivbZPGYd16Sl4rdSwjkRZtg2STUIKHiceFQcelOWJ4S7K9lNwsv4nfmBuZDozIcCZewjefcdY9NTywepsC42/aFXUjsmBvtc086CxadrzdQAepL5IrECWFRME6nfZuTqiNazcdfG9FGmpvcDrdQ+Iq/Ayi9aJKCfT3aj2zZ6NJeuA7D73SqZyH8UUO+J+olwuxRBFhHS+wVA78Sk5VWIEiPJX89K6S3wmnng3r62Zr1iYPi2lNw6WGFX88p81VzR3W7yjx7CPP36c5IKrbd/c4qT/gcY99lTgsnswP9GYEXd0nSSJ83HSFU8+wJdfuUt9HLIRVvgFvYMgTM92TPRJqSl7WcMRbCLSO02lSv95D0n6m8AMvxGg1ZKWeC16mBLx97B3W3nqPqSvDL3NcQ6yd1IYkC9kW+mLMe5joeNkEnHico2kfIWUT4a3GXr8RRRy8NJmwvi/pUrGEl7SJGNGCwN+K71/1LVzElgqVlLZmXH/Yk6FAxJY/gzC0jyqefn+sRJP/0KvYV/9VrtT0ZwgahlAvT0d+KM/Hq6eNle3iu9wpQ9IIp9DSesU+EBTkyEXDsN+uCwggs9ncpUJoC2uRTruUY4msaYyHTQMhuLXaua4bDYWQF9aSylFkjKbZAKpds27VTld6tSvy6MWz8zeh3OVtGF27PEYFl1bBx7TjQVskvbksAcPCa+u22ey0n+hz2Wr7uviFx8POF+up1LNe/X0xe1RIPsU58PfMxFfRL2YxnUzUg5pTolwEV6w2qlepgnAWV5EtosYG7p9Js8epN1DQLjNYDleOhNyteG/+D3zfWTq1qJwGQjJj0tIEpraSyk0iTMTvCpJ/zkbMH16aJ49DfZyjgXhd0cSkDJF+7wLj5C3b5YMd6WFz8kNOTHqV9y6/w3xDmVxx7fcet5HlHDIjo619gigBG2M4DqAGnXjZkIahYIs71DF9SF4kaVGt2fbdMIm5ziCaTIyhGAl5Qxc0zMRHN/8+8UFhjBGdKe3OwpBIshkt1Y5Pc59TetssrJsdN/F8vdHSD54HYSqYxJ/9Jrh8JXK2hMsIZGidNsMdsQBS984qXUXo8wHnMWKor6l9JJfMbuW7Rn9OMFPcAb6SKAq2xpXHzj6qBeAD/0s5zvTlM+ImYNUuUqd0ltqyr/278LdPUGiGm8TgBvVGwLcr5nLY140MyKCdLqE/b2feJMlxkVX5FHr3LZ5CIlXFd6yIfg2Oc+KgCZjKPdylTvp80EfLk9G3AlAv9c1KMA0X0Wqvf98rhTMs+5y+6fNVvgbYqkNdQIjiPCvWeV2tE4FLjIVIyOTPVkqsN4zfil+Z2j2OwkfkNFQ3GB475Gi0QBaeildC0S5+BzooxUYm3aySATiVPNN+pg74kPe3L+ZstwRNlSAKo5df2VE0KwwXBMYi5ZxCZ8RF8vpCY1CMVu073fbdveLl+wJ5OmeJ7tmu3fQRw268Hi4bkpBacWYQmegMqbwUHc34o7NbAe36OwF2+6GSVFi+Wy/tmW9n+L56qPK+tJBtNjB9wb1NdzsfLC8mBR4jHVcbkAu+hwAiQyRvCLW8y/BQ3ShfNPrV57qR/I3QUVOJNVVlnQJkPmw+D2MuMg2dB8zG0lJNxWV/dXu0ID/O8XDgk7lRmd3tJnXqseKb85JSIceUJJayjlDEvjWwUh75ewNv7RNiDZR23AKkyxhyHYSIJeWGz9xttr6bekco0Thyb2fW02Dh48Kcvda33t5J/SSyidUij8F47zHPW7yi9gMRUCPP8JNVOAsXi8sTmHjpaRdFUN0rvMs2blNFGoA58mO+riroTYvqeGVaRsiFgzg0eAhemcRUe1ZC7I7NV+y5+N0IUBnEFqlUVUUZpYEdf+kn8+TrV5YVzCpf4gi8x0RCEBMrS7mXNKWqW0z7sT12oIkkpMsH73J1kMV4RRORLBjviheRJ9Bk2d8P2IMQgNrFRphnC+5gr6WGAHjloI8DRV7c5EuRCHaofoxeR6E6oygnye9zEi8AYYWueC1zCfZZXIcvV06hwJ783X7UzVsi3N88NAh/rhIP0TJaSwlfRwpis99bW1NOiyGqO0BqjnBgd68f6Z+OBzRHqe7cWbO8jANqdSDc+WzgwXZwRb0bEuDsTobut6de8n71gr4SXN8M50XCS7VRb5Uglr8Zcr5QAeUF6DFN9N8pFPOTl0Xtk8BGPeIgAaHTEMKXYuaI+XEiwAiVJYIxL9lWRkOxMY5eaIR95s16SDg1Q+e7uskvXy7L310Ddo0oehgIejVt5YFQUVpnexAehCIAIMaGi9/7gJdgWCuEHiEgWm2VuABwgH7q7EeDtR+V0fBm+FV26IvNFX+J4bFARCx3qO7MAGk9I4LrALbHaZr6FINi9GuDKnMA+2wBr8a9Il0MqKUlU8EGD55nSHMhdpdPEvVuDWRpwaFNGayLB/Ma96JJoas0TOJZ4COlihxp5ZKX6kUJIMTMHUQTZy3uWQoZrKgbxwAYgOmai55y0TMSKWmlpZoUQ+tyfmC6FgPipuiqbbQXdp28n/hQW0EeduUmecbTq+uNnBG7tOHGpY4QMLliS1fon3Yv+kZ6GoC8Y3bspbdqWBKS0IT/SpHUXrnEkhLSZ07puu9l6AXjVRCP9mNFsAGGN9dzZaEhE//024zFxDXNycSoUxuoUFuLT5Ul6JiHi43xlr4Jj0vnQTbvv9NmaLJRGrJj/3qj94Nifk7uIBwgCVjssnskpHTMAXGg2NNIwuRdkgpsC+AiaIhy3oBmY0XELSx3ABUirDg+VbZrEbNExQC2fxEcV7Nn8wbm2uyberySXxWGvQM8VqfQ0PAk3nSuy7l2keB2SaIZS9ZcrUDA2EZJhQUNd7SxUoepHKx8xKQ61fRb68jDYC7QJglSiW88so9mm8emBfmdI7BLXXETMZGKdqUjx5cI14BRvt0h8Je5/iclVfK5aE0/usGEpevXI/0ZtZzAlZZRgonIHKDZjaFiNj2sYXEhcmdPJfNGzVAEf+iZOwKp1Rv+mRmpRkw/xX907yqY1DVLZEjU85IqhI6oz72Ord4tyC5QuHv/WjLW3HW3N2fXmLCCD4vgKJPdEoKXct2HshMn8oJj8HZxJiAyF4nZ2ngB/kh6kmdDCoCWFXJ5kh8jPq/I40SXPkU4YsplfDc8N72tlRWvx7/5Y31cpo5nSMbRkbQDnZCOAtihmt1ngxkCcwM9g0Ci64ujDjsBNSOGOg3gd5LICUepNvbaHfeW986aXlKc5f6ANrDQij7XAgXDWhNQ2Nq4+PMEvPU5ho2HYHIJjuUByoAfebeUSLNdjbyaVmMWv6DGrmttTiIg0/VU6bemHZn5v1f9+eD4c5haNtORwp3OoEwNcJgJwCDfon8UZi6LepuyyNp2jRl/n+oq/MGw47K2TA4MYsMD5wt4HUlZCtEeu8NfiHkWGTt6+tF2iVn5HW/ZwlawCLhqMwY8/PulJRdcYsrX5T2/O9hPCEkfPEmrGGU4TTMoUcOgRMSJ58/4oJdEDyUtHv0vzNhrjjlX7FhpQRKHDOnKY2JqIdgoh7JgBxl40Z/54dZ/YeHLgfFTgVHOQEMlb5r1Zsbj2BDV78Hl1rRY5ZsTSiawaeYEYemVwBPF1t5b8FDAPFjdi+nepvh+7GTZXAcczyYGQDNwjEekvhgF+eI/le/Xgsl3Q3wbEFpfGWZ0wiAy8/hHcQUnK6FNvCTsXHvkzQDgR9IUNwkvwvMpEFmsMcYpJ5sRNrBnM6gLToqDrxzLGIsNHk5nH5HLuOH0Ypoj1YTrkfD+mxDzTovupp2KxX+jLXhku2OJ20uX8s7XMBm5kxq502bvjvXlRBo1qld5ehplFqhqLT2igYzEoLpe+VgQuCyBgntlaAnuiDmVwfAQ5G+TevkS5Wc6wGNXIC28U3c6JqVkVsAVvq1VO5qMil2rZ06+RzR88OG4bdGzHYxEzv5mK5QB4ywx/zwnlIRH0o9Y0ZUq9OCu4ebkAWmADYbvzoZhSiJ+88mGmU6wsXKml5bq7qD/eKITSMxG85RrEf+6ag91hHAMig/yS0nS7qLJg8LYQvJ+K+QTwlWanTiTGPpyr+NGMZAf7vmezQ1x4EYeV5ZkKbfurq61NJ4ydK5gnU3Uw+uszSRRRiL/PM5l/xTxEuNdxzQ9q8f3MrAWjDuTmNJALxTPFeEI2+QY/KbO5lC/GizozHrKrFk3B40yh1MbDp9ybLf+eBeYbYPSQMPB7ZschLqcIKrQukYUvhTO2nY7QBQSFmSz/3TkS4dXpMt7iSTKoGeBECaIy6UR7ZdW+ZVT9uMxW5W3g2Eciio6bVfT5hqZb7U3UNIIMsjFJO//RnFt04VvPBdpuGNDFbYbBK2/efrUStsKoFY9T9gBN9sdmGAoeKkNDVHw0gsDjyrg0TEgCN87HsBPz15+RnS8C4wn2pZ/fbV/xNbEepxLctaTcZ8a+AcX2Hj8RGH/WeKBKfLVjcrelGc2lkwAuu2ISj4OxY+x3oEO7zrsdKCV7SBjnKE9mbdyqEwiyN+QKTJYV+ugG36Lw35YQiawjC0zUKJXlyDT3ICQhg1fe9d/niC9PkzRAFA5PW3CAMpK45c13wHdeymQfWDAKAzXshPTtNZpQxwXGxQssrBo/TpZF5hhOpfX0C0nFKj2hpIbznTOlp8NhjQnmgV28KWBpOyadXTK3zbaxlNtObCC4GgAA8NZkpLhuXo4AEqTQnUutaCMfnNNJVoWo0bIwOuwBi7FOvu6vwayKtYbfbIJKlFBigmv3ko8FVd6njI4T7vc8oJwvpwxFUJiyxfL+FcOup6qGBQCAhPt2sL6DodTZ8A1LRVV3A58ygzcqhQTz/cgAO0B0UWNOjNZVV5c017zq8T9QYo9oW4y44GNNFUGzAwdFudx6SPR2vy5Hqc7Gcj+gcdngQPu6X75OLA/FDSFQaThlHsPGBLPI8GLDSIBlADLGF/WL5x+17DLBLM/W0LMADUC5A3o8FdAPgixDhkt+pTqVSW0jrQN5sIvtk7qxN569fWJh9qAsHjXPf7FNtNd1Cxa5AdZhqoom+nCP7+zJN9d6j/hINC2i3ZyOmEkyzUWVQ6JYzWpGkttYeq9GMLIXF4wJ0nFJR+ImXkYXCTlipDfqo1mcP193ac5gqgZRmR+Y4Ww3N0gSSPCejUeBqpVm7P4vJi7Gd3stXXAnRry/2Y5yU2OQQhyL+OR9XkVxIY8ukENnW65mtP0cH4KGJyf1xndzpp0IvD1gS4C8sEXrTNSG2WBf/npGkrN7V0SrOWZDEtjLQRVqLoXKhnhJRSGmFKvdi/4rtgYSnHBNDafUfh81a+dMkZk6jBpcXAyD7Y3p1VcZ6WsN1ryi5LLVRTHXu6//oJgiRynt6Eb2ArmRk5DgOlbZj0td/sO1zKJu/f/q0+Gf/V2PwhcHspU/Rc406/YJ1P8IgLATerfXht2uob8emlcKT+kz764+ux6+vaKZCCSKI5Uke0Ml60Vy1Y2Io8tOxROLZl58u2xJFVCKkCTa57Hkl+ZE89g1IGipHvsY6hmeBM646LV6xCJWb6MFnriVaTjMjbbV4+SRfS+R+GNU5IGg93wLx6GL42wuFteYzTP3iqQa6L/976I3Hrtz/RZkYpR4r1UMKArRElxkuqgjveLtUfzzscOalrl0Xoz+1sUaIR9k1he+PpQybHgoPNWj7fsbhyhdNf82HYBdxFYuzJKD3KQbKeK20JQwqle8A2EiqzVKIxKZJMXdjVZ1v4g+yMU9YCED7gDmnY/kdtT2IVaStnQp1eMVIWFa2rBF3mIsOuF9zklm2CalJlKctain5sK+WDv7di6ZXKLUinclCT2qhUz03AhmQsC5qhjmrfFU0pDVnuJveTXsoeGKz+VF2N9Zu+1ib8rLeeDpwh6sUDOYOyqUFM5H1SEQXj3DrQVioJMrD4/uzJ1d3OalWz5o/XJgcfJV3gajwfVkx+kuRrQXEtVssAK0e/e2J349IowoEGBVIsApfMGN3jJ+I8BKlOpHi9CkMSYJgqoYERP3fhdCeqqMxq3Xn0xOSjGJga/SxF06JtOuytqpvWtcNLAZzxLS+c1T/fWR4c7AkqGvFkh/J0UHttsThKiwKaS3Ly7vQBRvQaMGxsADLjV5dTZjhpjHZvxNvy7AY2Q4FNHE5MOtAkeBJoxgRux6mhhB+5jkNmBDQiDkvZHCKMQQnFZ0haR6sdHY2Cz8ZbYh+ACqmp6k8Ogy61kUumhMn5/JfsaHTha6pl1F8hK69MoEFW2v7pWwpydEbBjvYCzUVCHeruK40GvjIWy08UJse7eAdZ6B0+xOaGpuOGMAn1l3aZMieD2NcV1aUy7nNZ81o8UwmBr00U+ROkyLjCPuK9o0xORFoRGZHzdy+qYm5Tcj+ihVpDbRwv2LLpSfQtoG1xVFGWXWKGeXRazliYtttefcc1WHjXTGtafrXT+A0UdC18Nj1+iEpYKpllikLY7ndHJgngItyYUxhpYdYL9z+BcrrLxf7jMh8i/LLFe4mBHmH5CN5elJyPKM+5TTW3VqWuXdqo4kdkfFBs7lH1Xc2m9XoZWG3q01x5sF8Ns+klLGH9eyGgwTcmIhEa9Cw7uX66HiQAgqt6OEYWF7/Rf5pq3SJH6mbeQvvwxVpgcCIoyqRkrzLORSHbe9aqUhOwemhLVW+Gog9hPor6WaZ1NsGSz6bQ8RdFFBW7biMep0HjXa7C4h5hr5d7n93ZpRktxJJFUIunL3bYBwafSlWFB5Fb7etXSwFcoJR7eZd+f1LvdLXzfRBo8p13G06odPDVOIbvW15FK1zTDghP3yc/Qw6H46GrUcS5uA5n7AxmMQlO/jP6n8UQWsCo9SPfiO5q3bCSYS5apudqlOfeyWpCbCimQcRjsX2iq8YXOAdYvbGAEFdMQTGlvcGk0l8Oqvx4VGhcnpQpSW6ujeYGZT6rFBM/EItt/EmLwd8esub+9eO26cytk2j8/Zvo+vcO8BqMhzVF6afd/JSf6yrksHzGimvF5llNIKcB6gp6QCd3tGKazVDxuCT56TwRB1BBKOxOhSmMU9SFLbH1kv51a5vx31ibc38FTba8pOtF5uAiDstPEbUxiAf4W/EyIwqok+257CwLFDuBfVCo5ZGxlMPYXA4vRwwpnI6aEoaUKT3/y6j15kdupSMdnwUCKv5GqDXSu7c5etbJh9rADpw1yI2qDSLUBRIKYeZfP7srp/aIguZZg4VqkXUucMvCksAvuCBfCidwQeHTldztqLc4TBopv3tZjlAEGllOTZz6E12l2ZDaryUf+ZN9gz/Cf5IBYylwyLXQXWrYNV9qWywsNG0iR9FbzZdR7cNyTJ21zfmlEvnaY9C0+BxVtZq7mFcZWCQrODKPMOkvIruBjVoRiQkYS3kNFT6PgLyrRi19V8Xx8+e938ohH4u3ysA6UGzvzp3/564yM27hiLBJcOe1iKkabDuI3wu6fB457YzQiPoYI1iC0oBmDGk7wtPJJC89cmq4dT4sevj7ipDzSLjDsM9peVRmTHPE9ufH2qYhjnRxDcWHvCOSy4d/Vd0olvEpA+L+2YaePqtmIxG+1SwS1e1KHBsZ9ayKfwyW4OCcwOMvTiWQuEEvIkRu2u95LWu4nh9wjtKOxngx/tO02c4DegpNeqUV3sEQS6q1K9n5tpkyyeeDsHZ0+a1SUxzFu3dlyTSj6//YTmNe/Slikp9hwfAU1bxJRdF4Aiwnm9VikE9iKuSz7lsLGCAaeIwwaS4dvo7a8s9NyKVtmrwK3JaRYjOi//N2xlqj+oiZIcgd/K4N3MmH3sPeIH498xsaiw3U+4hX7MKpW27YEnBpZVEF6bWPlgHw2R4AZwVfEGqaoRrPqnRve8pAhgWO7mJrZ2VaJsix9eMlMfz+3lc/1dLxdl2WcWDEVhYCB1PFa+XCVASLPRlrVnpZtFEmlId3/kadg7JEfu5Jngl8WpIv/FJ+bfZe997CP/HviTwuon6qTpzb1x6PND79QWOd6UtD7P7zF1HoA8xPi9x+B9Gf8N/Hr2dxH2L2zV6/9sYK6Vn67Bn36ugnr1dn7KXsFHLDoP7GwK9S43Ynu+dH+/deonpEhB5nim7Wl21uBnKv91vQ1cauj8qYcITGdX18qTt/kPezyX7dFNmo/7dBZ4afftsd0XLsoQYXUu87dlDDIcFDXIce51jNsi2IEtwfalEomc1/ARjbyIsLS7Iz9gf6VT1vhKPbda52aguw0bwb7RlIXRzXRmGpP1eqxK59vP8otv9MmwvbchIl3VST6MnxDhOgyAdSXAGwhxFHDREn2L3FbULoB2lM+ap5FC1+rRtS6GcQ/IN/rmYjgfDYWMifDh7Ce9YMnt72vt8dq2KequPCXgsm2erroidg7l0auOWcDSk6gXrTm3RzFAQ2GR9beBLUfGQs6pcNjXn4BNdsLfo+sNGrlrvRJw/WUV1BdjeDAFvomHkxoRssd2m5SIyo2AT8gU6EUaeEx97/BogrdWiaLY/BlC/PIAUKY9+PqizwrdaJPGPEs8lll5fOS6ArGcN5LG2HGNmtQLm0c4J9pli9Duj8g0ow7wEdBR7Hbqiu4HHfjTewipOvLPX2rkU3oB7HaCe6hxKAbbDCAXj5cMgsdV7BtcNpiuay4bn/XgXpXZKpLDT+qcoswQIXQt3DOlCfd7buIRz+gbsOBOTOkPdWyX/4eqLdxj727c8J/wAoLejbFSnsdvsvDQY/1F7fXH/f03sta7YhRBhILbRuOc5rR9HDLTJONhQOvxefkQT4TGgBO6MO+4encuYBrI6MJA6tOS0BzrRAnbKMlsxv8Q4CmbJgcVI0NvqFq5sOu8a18y7TkGgNvd1s01KvQ9mXK+a6zftTQnQdupyYW+KyP6KV/5ideOSa/XNVQh9zoXRApncZnBJ698k/NwVLWys9VRW0CZ80QzOZyX16C1xjA97t56wP1T41O09BwofBV48PWwLxpfieFykaD2OfFPyu0sRC2CBbxKj/GV0gTk0rM0vzJOfxiR9ABUOqiZkY6McxcMNXM9qGfKpujxB5YcxUI2ZnQj2vjJCki2G4ldgvfGbN9w4pvgwWwboF6y0ByCwTkxq/gD8+yjP1YfR7MhUNg8lmasbuBInQ7Z3Wqbnthy0fxGA9/t0gRuAzIF8p4KMkegra4gl4AyBL5coDldj/7mylW6oHz+t7Wrxkck3NKZCCNU3iY6fNdBhFWnNKaL4tZQbVG370wHbv4z/Pb/f3h5/kes5xlZAP+atIl9QPdCsLA96dcFtNsyjWe9tHf6/1qyhdP1kQ1/Uk+XrHn2uIxbM54vZhO+L7ByLdH23XeVSOrQsWGfbAlhyxeCZsil5QglK5U0LcwiE4jB6JIKUcuWFr20psyyrjhnNfKc8424Y+HaaTsBcb168XhNd694D1v3dSTi/4lXd2CLQ1AUccQC9sicmLxS4EPmA8FYuFM5QkrOy9WBGb+5iofDZAWuQrnJgz7rREMURYxJTvsBvEuGYd7IgQWfVcDdEPd7fGIuw29pDnT1Tld7UXJDK4vCDa5sLoz/8NTVDLkpo7IZmkzpIaRggjvVzWggf/x3WNvEK+A9NPIDPEQnGpKfmDw9sAzt3Z26eXEglOKVMCq0+fW0lclHR/6PG/7876Uc9O0ZI0ivExqPmC7ETwWzqnHrLVQqEM5efyYfIEsT0zZ134EVn5RVO2PFA4K4YGJ1zKhtpYnUUXhxTTlwGvOA69PHiSOhc+xzrc++eDqwrPwE17Mf3KnncLpsQmZF6Av86flp9cP2X6bwKT7V95fDwXJBG60DKnhdNpji3g+ZIU6iGxrKpcnbRkpe5/7e/pichy5YkeNGVNXiZlZVRLPWAkVfrwZon01X2h3QKnXVbKZABSTytgQfmcnm5PQ5UItZ1lK4yl0RHj8hEMx9oD9A9MZ4OJHSsEQ6rLdv416mpDjCED/lpTTP2xv/1rdL/Tzu2N/XnXRQZHGKEX2w3L4PqFBfPPYtvUkaO5EcDDWXHInD2IoV/1ymhlRO8lu2ZvkWQmW8xrMaBN5k4YCRK8MpD+P0jANVONUmmiJtgr2cTZaI352eYjd1y2cYgKit5nrRV2nJI2k++p/XUCp1+6GCNJyMZk37FgHDdAyQ8D7SB79Tj2yt2lunl9lPc4+p03x6tiDHXw5zdpOXrN/hWw+YQ/72q6mRUH9TcwQzMQoIybePPs7GCT4JmWD+byb7dDu2DpNhHyoHc+8UzBGayAEsfpowaXqWKpVe5KTHHvpNsgkTDoewKbIQgHCr8/XyIA/kwnhu1VNNByJpp1cP/Xpn4iWTEX8gCrQldILxB/8XwyhWggSitOw5bYmf7JBOB6gwbYJntdjvXHWaHaNKPvXcU43inksX1vGGoroGYtvvx0Afbjq0WSrEGcUTLOGlfo35BMDVLuL3Iwnu1j4oKeFaISzEdabX9F4TCu+EvY6yXu0ztBIM0xS/ICtCOO+WEPLlB1SljVQbOhbwXuoTSKsnL26ytO+MHbZ3K/g/AAMYN6wEWtlaVYbyP/S5U5ic4CjLUrLaBXT85S8eMz/oLTuus1ZO/UCz4eBgDpXT7tK/4uQWz9NZj9e9MeKmzpfC/N3kvsYZQOYgQQrFpeMFJyQ5obJGfrFJJhBuPvLbMufcadSAIdHkZvpTr8bQIvc7pIAlJA7jfADkOPcTdMMJXoeztkEecFhYVLJg6auTqJv3jOeEmIgTRBA2Pj4i5RsH9YBEbvioAU4uy08Hat7YGAvBQRHNLNIwV5URAfia+8AFWctEfS6s2PicEFE/5a97grNcg2F4RAX7pP82sezY2MQNjd85rfJIxlgwrTLCd1HnjUh/Gwn0eq55/pApbVAqucjT8SgWYHU57IJI4YkX6sjaLrtd40d5A+NLCaNIeGutvyOsJ8tCCfhYD5d0cVDyIfy4smhDVGD82T3+HLurDBqXO3oralLNhED3bhKG5HhHHSoANrgXZDM7UQhjtvheXgHx8BXAJZPdtVuWZYmKYcvdpb0sjZAySHcY4w0onWBqlAdk1BRhXJqZCMUzLSt4NfWe3dBRXehDlqlthObSjFw69iKhgFDusJeSFnjmjh7fkh0rkDGEIjVREB7gxi4X+0OEJVgxCLGJCredTRy5MmbVwt/5ShUKvwjLUUQQQXHZnfFa4BAqYcUvyxc/WungRXUUpxj5BhOM4EEYOqFdu9Zmw8ySm8+bcCpWrwAe1eZ6bupGmwLjwhgABZVH9VB8wH+d5mNlAtHPWtHm8Tca4ih5R8ZftgwOugWsbtiEe8Rby0PnSP4shGcIh+L75NZDGJtMScDk4t/nkK19wAc6iCNklRfliW9K8A+Cwmu0utvAH4GfhVXprf1HrO+/rOEjtLadXuQ+KrEOg7KKBc4fk9I5HoLoTw8pf+3Z3AK1niG/CbBkhCyk/oJKfhqomr6luMGKXZXY454Reepq8A2bi3dNRtYaBP/T1EJgLjrCk1yHlQbUMstwTX5U2Rl97TzNvLO+OW/8Z5DxzQBdoV0EpAxBvK6qKA20M04xDSazlZNPLgfZXkeCZysBABbA5V37/2Eep70GKHlUN2NGLV/e4JEK7x+NSjKfisEOwVffbEk/hb/6e+BoQjaXY1noNreD86cBUmhIbQ5Wytr0khcqfuvjij4q+g3rRzjjxzaJ/LnRDqimUzDuDbPIWMo3qT4Qv0e+zQLmZaW6PWnc013onQbMV9lkSK5bF+r+x2mNzl7yFn9l/7v0mlpWM4pTduaUlWJA7kkw9bVRaz5/IY9KET5BmopRvG1V+gv+C+xH01QEPNWNTNsGS6MDnI01ZApaPr0BwnIGi3JHx2LtXlul2usUVqudqpaXWRCZzT5iYja1grFtWD9MLwhN61Nj8NLKhIVDFOBRxdDViCzoVpE7qD7caZd2dGIoxQRnebkPgMYIC69j/W+97NZ/pQP7MNJa843VIHcaaounXaGcjrR1JHq1EkrHvNAhPGrOwbQVu/V3LFp8R9I3hWwEbaKGSi62Qbue9mJiYkp4Hv4QfRSTWGtdqOs6jlfrh8n2FtycdTN77eMmkg/OggP+sd81LBYDghBACHn90LiXtohzFENHmuk46tBcNWQEbqYqTVFBvmCwgbcoXilipsXnaIU8llT//gTZk1Bwd2pOyUh1pY31Ue7MztJQjuRehYVB19SlQpjw+Vyrx9EQn0YszlJKmK5/rp/Zr6yLL+Rn80sfuACWfGayTt5mFIUw9VqTQe5lir1LsXTAqV+LqHcFI6UQp2T77kmW5QOIhv1ZDTu2RfD6+x/qFayJbMI0ssXEBY3kM/iXBHWCzJuViSXpcuWxRrA3biWgme2CcptUWTQQEO876USdH4V26yDbTYvPYby98fmfpKe+WdmsO0An4yJqFmKPEN9RFTNC80rJLjaqLLp072KjzX8CNjEVny2Dlib3joOD4EGN/RJLZa3WLu2nomC14a20nyszRzB5wSsNk5XseeLJR1sB7UAcH5smlY8v2akLVShxCiKBenf0tBjdgIRf4ag9IQ77uFb6AzXs+qn6tEc9mOubITwW9tDJecin08aO8TiWHxblZ6k/udI4liLGNLRiORfCBb3bW4EAAC0KxqfZb5KUL0N16u9zjXR1rc14jkExp60iLzA0OTNeFOFj98jJl/iVP158Lhd09up9NPiNwlAr2JM7MPMZQ7I11AA5MO+NsGJqM6PTd3xWrjEKjQk7wl1B/PfbR8Snl0dJgsIE0fkEdQpX6leW63Ah3TeMfxFMooO7WaNXt3RuTyVtii3X4AcBO0MUeb/GJjyIZWTHaXtboqEklbfs2odgPXdaB+utDSazURYGR8Jk4YC9XQI/+iSbjKry7pKLjh9hnjNEMuId6Fa5Zgb4El/rhqaASfd1yNVs8D+cfLNUYTWwLv/rYBarphqhYFogCtDTJsSxPkpgBdY98ZyXjvVpQK7hNY/cOjS6uzAVv7sfzjaNwDgQQMCMzxz5Hof6q0PZK/+553TiwZQG4lvZ73n10gJTOSnbq/88gc7JaJZy76Ig+2Q1KCNRqjFAzbXX2JeJPF+/deg6EgU9Z/IW6DU/lNQmkInidW1k0xpzJl3W6PqeJaKIfTrMyifVtwIlbG4sb7aNYA8wVS0i9+NV1dRtW1GTXpnIEZo2E48pIjvYTtSRcPo6aXrsWDcx6ln0qd1MhZzJ9b2o5M0u7usWkjWOu3n6nWnX7OYPV8eaTFfSkizCpaTBPLVmrzlK6pbh2agjcKTRn8159hMmbAApYHZdvAWaiTiDikU5tbQ3XSfssUJfvCeBOIFTVSA4Tol6QLY8lU+rbsqPC+CuMg0k1WCK9QfbCXYm0eKS/HeAO1b3irotR4WBz2J47gj49WyEn33qsnU+NALc39eT4aq1AjKgxmddy1wNMKhtWs4Wta1ekgR6m6VpQXJccBPh7YHdcIaaXPcjmUKamM0/i7nXP6e0rHE/cO9Y41Z1sCAzLY7z9gpAxEm9fOPuxknMOpTMzteaPQSiqThoQSt/Lz5UdpJs8S+sfdLVYX5nTsGGoArzC1WEZr9rzRx72BF3xg0d8u6B/C/zKFH2VTjnqXyzDF4gyjYPhSL3u+dWT5p5+WtAhNbbAwlVIYj58SvZ6RD2oRYNXqLr+eoFaJW0pjmO7NZSfy3Omt/br4vs9UVjk5v9kZH253iJjN8V4ENPbL+4ZIvj9N4CKRlhKS+3lX+2vvZEOL4BdEsdeEo2sbdqoEmViwKYGwfd07hK/maJh3A9z4i3ig2FRXXw+x66czoJAGGPJkUq0EpB5vxesmTiDDWCmCIv7UkxzMVyVrf6OftIeKMhiwX0nleW2SagOUPuoH6XQtqPsbSQR3CxU73aolnOnGSyzDP/cblAmLeYSOQSPwYLz0akAYtWu/osyEx8xpBfjZOrI631y5hPmE11HLjdsZCZKVshQYU39RNz9aPGUw78pcHnsJAEZ5c9a40/YS+0jJwb0RUPO8SeZqV5bisQ9Gm6RHgiw0ecSdH5DFv66ZiYn0dBhshuetAtCUmqBQaANKA97nn1A9Uj4sx4F1Mwq4rbqOq7bhCW0WUgU3F4LR+CS+lwg3ghq+bgE9KGJPsrakbrK+lKB9DEOaiXdnSa49IQZkqTsPVzXOLuB4HB+w8MFYTlyoBErzHLqZ5O5/HYO5et1gftMCrAvgB4JPmRn72U3j2RTvq/4kSfaFb3aILj43YlWnkllnMtoDPgMQZXA1i+2NC6QkhYMHrcR0cBfnyRT54wgs9DhWOlTrln5THPKTDNOrJPqN+dBK7rtsxW81e6a776xhQ3FmGDmihb7S1njUNAfwTg8nwDZPlNyh8xrq+zANU1JLvyfDi7cHNAuJvXnVeqNBBZbTEfdiHh0Xg8g/lC7xeSncmxlVlFoAXSiWxArXmxaMXuOfX9x6JZMy4S4yIRfNQbDLeSME/lMa6UPabTtocik9hRPFJ5J9oTMpEKN8aO5DvJLO7pHvA+gXlkVC33bCW3ROmrIJc0Q4C+PSgm4y3EmUW2MkdiXoFDBwsJevjK7k4Ni5S83pSuMSXH2h34NTM4ShEEDQ+bKdauv+DXCAdldXhXjJpJHUhrODNCWIt/8ujI44aCKYB9YHFd1/Z3bppHxeKIgY252EUYR5GX+GdbgIU2Be36NSuxnt7hkW0GdYnq4zOmL0sIOBCgLbiH0u94t0H7/dYhfOX9eTvHrxPs35nndWDne1I8CiJTsmtMothzXV5rI6JtvfrRc7WHpNNCb/iJOMX2upsQ1s7eSBpo1eTPcT4rPXYWoDHJdUNCMRDPxDawzstXdB1EHED1t75vdEJ1RGXjKfG8WlTLv/iFNDPktoHoH+EPeQ8RMFO6ZSrdhTGs2u5u6gfGMnsxJ3Cs42thHS063w4pcqLpORxy5ajwFVbVkDTg1dC3SbA9yjF/Mia/cySpX22NdJheV0TZNviPL95SZjcZuphW5l5yBux5KexFRFOFyK3+nz5nm1rG42FSkISvBytDHw6Hpcum5hgmnyq+GouYwgofWuggxoWW4hxL5za16bPjDKH0+nTYvch55mOmD043gLIAIsGMmwmJ5Psm2doa6uU5Qeu37iDVgBmMdpLXyH5IicK/oKzDhNpFdhnusaUWw6jytc5AVGZFJ7loBR84dEJh87EYp8BB5KllrDah7RZGXvZWmXoHObmH87qHSPT8sjbJruExwPjIdIihw8CWjKa4AfjQcPYd40p4ZOlZuooYDq0zoOIls9VkainjDKON12brY9Lh6oFX2TcEHCwi5HV0woKW8z8zCe+7pr7sQGceE+HNGmjxTcn01xpIRSsGN0X3NXcYEQzPfRcPwAcLWkgVnS3ssLt+Ul2AEHwLl2DYSBLlJqimE2HcRGVF8ALtTgIqTNV14rUGhGUxzUNmW9B1g4AiVLjZYl+APO1kVWp2PcDkpg8a+IGwpm7lG1XnK1cY7ASv8TwjPsCjbkX9/UKmWGEw4Q67GBKT9ffIv0wuFacwTCDraZ2Dq5gamd20Y4bJO21sbvQoYtiIuqJIpPNUCpO+BlI3LZiW0q3IlgTU/PpdH9S0Epei1kVbTv/8A/CMNM44o0u2fedQ0ku1Npar1BL9HLZTSOvf2ExaOGpBsaU/oKxWcx3E1OcysdUFrD2QxM3J3nuWtsr81ope5TuidJdaoJGe+Vo0yAzi1nU3KmK4Ln4Dhc3xgYtFNQrGfAB795pbZVmi8L5leZKp8v1ef/h+bMbeRT1s52jEWEKHJISArF2RkfiTaLAq8JBSZI5vaFzv1FKWOdRdAway1pvK45fnJP5UZmtgffLpntC5T0X1vQ2oRoIo0Cmr5O39AuxD5M0mFr+xcc1GyeRHDSF31xN5tLM7Q7tkgeUJEAPa8n2in5S436kduf3DLgFwMKCz7uEGxI5iR/cp9rAbasf/EVNvXO+YzW3UJ1O43ojToMb2Yyvw9Rs2VSex2TATV0cPt6ziVoxs9cyNxZGkhn+xo4Z+nYbjkU/3OLCO8U41MspPjsr67O+Xsx5t27U9R6TfOcRHfSGIDK7bblDgc/Dg/Clu3YJlFWQ6Z7gECbAs/mZkDApMWYGz5nNoXyl/hzgN1CWwhf7aCG3u/LY9R6QzvkgpAlkwvRy9nX9HfygfUFiTe1EDACQjfTJjzgp3CrdW7lUxDNPgo/bwW7fOV0tvLeRTwZDXFOFVXS7r2Gl0wwWFlxNJ0yshubY51rAIJ1Z0RzG4nCtJzhS3GdLnP3uUjPyeqrVGvu1rAOO0z5dSWTq4QyPISF9B10ZSitsLy0HSllyDppggyyICQ2hIqCRWYl5g/pIUSVN+F9i7he3qJIAYNhnlDe0kRZaGArvCtAPGm3jOdZSLvzMOR3LR4TaKaUhxR1pIp70vVn3kJxNWVUESn4Hk0OaCHqC/aOugPwe3ZPJU/QzgT9jrw8brj9FlYuEyHFUe4axK3UQU/DtsrDq/ZTu7/5rsVbr+1krG4W0Qep10m2s8DbUqY8jfv4iHPf+jdFNPId+9hiCQuwqbbBdN14ql/nfGyePXbdtD8VSTjnqqdGU5SKWjamzHCEJDoLG8uDDyy9TtOb5AWGymTVNOF8tXGgSvMRdHjB0s6+BevMvaHSLCResD3of1iu0cG1nCuOpP9xaiNcnFGnG0vCoBxBn09rPMtAGZxpbDT2qsSskuom4yHZRCHLpvhg8kHY/SDNxjCl8da5CoiLERVeBTRpj0qlfG9+qmFHJS7ZS+VydlRAhHOoa+Y36BYJAOkjTM8pKYdytFDup+Suz+nGMRW1Tj9wObHYiOnX6br463l/WstJJXUG5QCtFDpeY5T2EkwTcZyD8nZD20nXBD4euJqb2VUMY8oeVvAECJuXkMKhhWWzhY81jV6VVt//G7Dkoh4mQ+jLn7I8uq0HuZu+GyUNc0HTOYcW/lW+qSXQgQzDl8mJ29PnCYHdw/xZdjIBGlx1rW9CpXOVgqs6+49dDNlS38S/YH40N5Ej45KG1vG79W5ftODbwikloRE34fsSEnWJLgvWWyreC32Sp0aG8IxhrlxlWIMenWC4v2o9dPcCz4rVGbrKLMyE+5qNixFREoJ9IXbB695xsIjk1m5v6zargRP/N+3vxkLHxIGEq3O3guhSaUvA+ke2nvIVK3vlAaLhBrFcquTOnpqmfjglZIw6imp1r6aZsnaHwBge4gZ642n56zP0JgbzIObAX9HzoA0IZZD2eSRSpPbVA7iip0SJMo93EijQHSNa3iE96URNi2eRN6e1G9W0+uJnH932Ba9V3wpg0YvuVkSFzkseKUBnva+ojkUrRfUh20DlH+QTvEoN4yvu++s72bvBCgjHKw09CVYfbgIuzV6h3JRbTwoSzkJk7DVjZwtg15i3pVvnOhK/tQp2XcMxY59vfEZ/MFcP5YTFK6gQ6n18kebXLtuSnjTTAtQ33V5dEnYChnEmPDJg9cuXuMhNjCJlrwbemri/q+doEVV/3zbEogvgyv75WUm74huvgOURH7sI0lt0KMnd95LOeL4rNJCl7orCQM/lGguoUdPx1wYPfdbEsML+Lqxs3U5JBT1gioQca526B5Nsv6Di7vKDM3XAE7wu2EnNjF5ldryqgJwEXdB3WXbXx5Fg2cUqPQZIoA2FUEjgTMK7KlvNuBabfN5Q8wUX5vaN3lkPYCZ5zwP0XjB5YiWvLHnG7foDoWGTgRYsBdMlXCeZwAXMBsjpwHMAwIks2P2uiE+5vL+n0s88gY/wjc47CIEdbEUd2naFjpg9Op+yzIaKmysMURlE2hp9QIP2ymg1SVP4+9pYyJ25euSfazdOVbmtbdAY8ujZKcOussZDQDrvgADMjTPlO/iJM1FW7MAALPJFZFxJoGdhPGl4xtE/OtuIoQ+ROQ04ZOyBLl24AlKHpagIPux6x+bPMfCmB7bu+HLYdJ3Scy3kph8sKI/Q2RBlHmDLDZ714Rd7lZJT0m4twBq86mt6bp9wQ3WtXhFjR7qJWzqVlh4jvwcX62DzYlclwsbu/l8bvq/G2g9DZam+0LcoHlVtgIwR0cSt8ox5v+qxL4Uj1kMSEoEPRUKhjaPgAPfJRwKJooLI/Wa5QyXjMSiFBHPJKM7jI7zSELR5H1Yw8o1r44OT+ay95ztbY/q5paVhUqfa5KvaLZmPfRCPpio0Oig/TupFNITbElJDbZlYRc6Mu+iOHvaLELcDQU0MXA3T2oKWTWalNoXHAnsEGcHDlVn0oLPoxXZ/t9O4ZFu9SK9g1Ktu5LH0FYdeH94fh//ROs9uwFJioq1ViJTkpZZfu+24v13GVwOQR9G5XZzjP/DMlKkbqaJjinpEvVVUBkv4Wr3yln5em59EG2Pe+k4ZMjMloyl2bmvOFlgfsK7zAVQefQRKNNOLigWA/MNQlda1zbHdqx7XhO6/xfcyg8Pc2OqHbGGrRxIB/JYbfTE0sm6LlURxnBD5QnHm2ZXsUdFI7oWKHLnaqVIwIgsY2lpGkZ+r8C8LPbJPlzeVz1eBYa3usT1NxuQuqTL0DKwHaHT3feKNQSapW5I1f57K/DEAgnp2uPrACDaoE9OsSR4TYt7XfVSBrQ8UuX41bZKbJ+We0ZAt7mpkknJzDm8KZOQeTEMROXxwzcy2jXkp/qeOuMTow9lmELKa+p4hWUdfHyE3g4qCKNG/h7zA/VbSE9Tmh4014zZLwZ87LbAqp+pPAnpHV8CqT4kC/jwqqCkSgUps2p1kX/wm4oLRoxHCWh4mBv3uGyRLAQ62KUeoMx5jjob1c8KXQGr+CBMAE51TByHd+1HeTTvVgcK/fh4KuN1wn7awZ8LpICHrfkuL5Z/XbRmE1B4reRNdTuDKjTTllgmzxDhO4kRAkLmAkniTvWlUAinjaUrNmN5iaHd3T5KdFtZcM6t4KRvMzatzie5polBo23cT0pojM/JptYGVG/rQjM/TxlM5fmy/XLkkwgvy+rkKd06QdKrPo0n6VVDRh2I0qs+ttSRL359XQDDx5L+9ytr1wbdiPLqccdB+3LmV2AtgGZlSQiB1ggbojzuK0wiZU8/vVGy2FMHls/SbbbcGU5s94c6IVMuHn5XvRTELbZo8KLWc4tFgR/ffoyp3yOud5BGgOWVzq9PSx/DxvwGIWa+Gquqz7Wsfi67vtkE+pNauLAfSSo7ad0/V7XADS4zl+0zGmX6nSXzkjd3EnRm3bYLuUNU+fI95HW53hAj81tnABewNcqyfo0NcD7ttBelfM18APjmx35Tjf7uVR0ytD1prVkca4Gberz57vow5K42pCReL2Ug6ne0ia6/4XaBokgHC1EZI2SUZZG2MslQeQuKq1vzwLBzfV77rt73LsCXNHrpMOMS10sBAClsrc8oXqV5/J9SHm7YIYMmu3ntZKby3HNV+wfhb+g2wy8ZbZlbHnJGjpALHhe9/QpvmInDkyYMyWQyA09nIoFFOZq4VPTG2UKJnxPHVR0BUxL+ZFy7ceBv2eJbYyqZILDS1L0yNn+fgNi3ssHniijogj0iED98qZns2zO3kBSc9QbAhw+VuF6cWq0AUanqa+VxT+zoHl+tL43HZN2KyTa6YAk4RRB9WC0mhH+4QvbvmplYaFucBhqyjcZOET5P1+9j98Gg1xMc3LkKAk6nsc+/GHXRum250p8GRmHmaN4VDePhckj5pk7wjRUufKyP35ThaPLUuSlmKjp4yMHsrykodkvYYdx7VPzXAg5ek4M+PdGuXQASLORGqw/Mebk7k37V9CMPrFJ1x2C/uuxWbq0bhb1z44pBTxPHg1oW9eejkifFQjsztAQN/ksIryY3qdCGdmzlEzRnPUuEZZd+u6EuJ0una0qAGwYosz4w3KGPnkmKb2LA1OEGgUcJDMYg1S/TdsriI86bkuqFEwu2RT8xnbhh+j4b8Ebxcu1CM4eNzAypdISW+0Z7AY+we06NHYElYdmvwc/JJHx7wu59eK3AO8Fblz9H1DeHT7Zr/6cD9Q3uL7f+fs5+9aW00ae1VehkphVGyeJy/rXmZKsQ1XCJafwTK3DJS6TQbysd9XSQcbwvOA4JTMKooLgvi3z4GqxwrKR1+6zRI/OcGmxHIrRDAVN1AH9OqLk6+YqlE3HVx2+ZN+qq5E3Oi9B/lIJBn+OwJmdMX32A0qf9rngINdyqj3v/FIoLShSew1LsrwhRF1jW8ZsFZmsB3UecscD1PWq8xpLKFCfe2y04J7FCzFIUK6gP6FTgKxq3FVOYzpBSLf1qKOCGBog/LmsSwFINCrVmNGjHT5k74tbjzPZRr4NPas6kbm5bFWubIVZA0S9fB2TRy9VYFBZV+XjWMemHvboZnqTMnNKW/NL/SwA4NpNHvh1PZ86ovjMDHq5ctKncZK369r4cprkfVNYh+n1VRe1OaNjDm5Shbg32HQsC+rZZMejFdPW95kDXWCuPxofv0vZ84H0yeL5dbzTeYpgdIW4b3j0mCgdReFpBP+XPQ8+FskU/BTrFEllPesyGczlEebCH9Uqe43l+51EsDVe/oPopMc4W50zhwdguGnFa4ECk4RLaukqdCt0ZPzK6kP0pu2Dvget0jZFP8JoIWa/GyKZkqoBrptRLIKqcdFuW9H+T1bcdCssNRUOGc2/09P8eRGKMKdY7Nld/IYXLZKzqUcT5nzLR/wwdBzrk2xFeKS28BTxaHx3w3uBkmvjV6gtR9SjWfugjEkapmIxxEAlr+eYM10odDDkS4Mo+Ut3TMJ6pTQXAi9FGjyqMU+KizfkoDGSm94SldcUAdJ+x/HKX4XrG7faTgBAbEhIwJNlKtHzbMftuD0MWB2ioMOQl9oX1TRrUkfsNHeZi2Ome/RcgpU8C26/6nfCvg53k6BQuYB2aWrhEcmxYP9cu2lE04mqq2iIaJnG3KI5O0MqQe0+qufyUlFdPLxeot6vF6moGAzoiOG8iPI+EiZxH221IbLcaA0qaomVJMFhAuyfHa5UM/pDj+iCvSsbRNiFCgAL4YYHzlvnDxI6nNGKXltbzOGlBSHT1d44lkafPW/44vp/AR0NsZg/zWFN4BCpCWQeJkqsz4unvlFK6BeV8+t22pUD8gKCwpCF2jH8U3pwDLtFj60g7PtF8pyn3EaZBPz2HrilDCd0Bvprwi8dQtfdqwuatYIv7Dm4R8hvQfR7ZgHyJ7CbhJ5xXjajciP6jEc+Hl1yZ77i5NlknjRly9HGXrBhR8m3yPS2lt+Mjqu3kpJRx+tR8SNaHew6uXrLI4Qeu0Dq6eP1cpcOStibDEMLYAMDhmbS6ykdTPuLFosKPzaYPVHY7HEiEsIz93hCRyyOfC7ZH/zAOakUR+xIxjtzMJNkYKCY5Jji3s8rKk98ewjx7ZXRbWM3jy+rherGOB0FMxBotKDvpv6BcL0cW91BTBWsHU/Tq7kwrIkUykwbKizmKcMQlCius+r/K7kbuX0S8Ij5YirL9ug8CyUoDG3eArgCitssehZrT3yK9RuqN6rFaE763ULOz6M9DsztkbTVw+ljnJt+8xMhJkcsQQusIrQxvCa2Q91gsJq+JeWaRcTPPLm6xtkv5wdlMloVOVeMxMGNFkdV/PGt6jr8Bd2vtWr5Wfkn0zKGVrvffRcqD72jgjx1NA1HzWw7X0HQUgGwual6dtRvCXpti+rGy0LKMkNs13SRzbRBn++fElrkgTScsGWbQ7NSJ5vbA7wD6najp+3CqP+hsx2G1dbbbfhhJ/MuxPNgpNe8x6GDncmoQ98payTtMdivfRIpkEJ/SB3mK9RbxyUX+9ugyz4W0+Z3TNjdrE+W2qFH00Z8Wptyg7R2JGGxkfmZ1QBEADPJL3r75/fHSZZBsP6k3VDSl8ube0lYngZnr3wQOj7G5k2aeZdB8NUlAkb3dBELzfJ4EuAOR3nslSqJkMvLSksRCUg38XC/1CjUerwKaD4d95S35t9n4bJjsXM6QFrTkp7kvnvfBHLCrD417g/3WC3JCJfzyylK3LNx8XWjc0/X6PUktsBveDw2GR0qTz9ZC2qlAGyRqhfKOPZ9Tcq/LjYmI2ZSh0iiQnhhuwkSXd0wu9tlN0LhUtPgvuC2/6U3LsnNdyL8ZuKZbMX8CWn7sNuOscv7Sgp+bX/bZgggvyGxgIkV1Cy52O5uay6A+U8s6aB6bkDm2bLx4TXs5kx9bws/BZctrU9+vPqH6UxHamPBSefygB7lTG2LMaJ2oaRvnv/vCA15LW7W8QTV9keaIha7jGEdyCo+dwHrLM6YBiGQJqlKGdY91UiG32ZuE/zMha+ccSAlj4U0gcGsrmKiEv/8V6Y3UT9gO4cHUrpgryGx2HubYeFgN8j8RnanhkLR7Ed+Kn5fv2nRfwapZ5JJuo1kAOB/Lwlr9059n67LYuT4gJv0u/Q40pSwOrB78e+1rZ0bNkFQXz1IaMHea3EXq65k6diwLlZKDpQhOyatF/a7VwxzGfrVNQsHu5PjJxLWKkCVCh+gIy8JVt2IOJURMJpasJHJM1YuOHcLHIFnvuXMzhUugsuRD9nb9v5iNwrZXiG/2gMzUn5qc0gaX5USoaMnvBJ9EcCPratRTe5G0FykfN63E6mwavI+GK83OQ54xFEYTBoGMmTU8hoG34Vi/DLlD68t0c/fb9pAxZkylyqGkdm6xb10ukd6VQGpmnVvrpquPTlZlXjL8ScVtpcbqpNhkua3Eok5aLGTrhOnDLklbwGLEKHvvES/GSE/jb8NKtTKrBPXue+KxlWPJtyWCq7crqoYti0EYxd54LK5qlzkzA7dIwQnX4WoEU2NH/kS7dTubi7rQfYPhvgodBNvVcRnet7+bl2g6Bk1YoneSISAzmSADR0wC7/rKcGgfIadcarrWeJUgaZSIz6MM14jYb/CcokbRTZzcy+PLeufDrsPVproOZEY2sEp1cgmcfAv1262F2YOepXkxLFGszogbZSsYQMfO+sDOBIXsRLd6bkBtWXSTf6yKWTesUmy80dMdzD+Ui/KyEzD9h6jqxtInHfRykgZ0mBuMQyLQ0mkBwfK/aFrQ+6rRTXiR1PF462iq3D1hG7jTf7teridUjGhn05nByCa9JqNCohlYjQO6NCatuPOb5ey27m3eZq8Bl7AKL4mA6Q+PaLs1T6PO1Y1RHedkPiy6DovSVvwHXgZpiKuPGEeIli0X0vwO1XNI508JDyAeBSzgtyPpwugJbaLuoYlsYa3poA35h0hlK/6Et9Jk9P//JUe+hSKjFRg8PX5kfSuKjEhfeKb01VCExBBWNYlKt+RtSaud003x38B92JFsO4mpglRnqqAXNSSh72BwxSx0hfACCbzO8vShvmMBrkoR3elZ0Ugv55NsP1nFkF2+hmOiktV+BtqdJ7TLescR2xSvEc+FYZyDuPLr6MriFDT0mMgjzytmExeDRx0t16dGo7jsCgLc1B+573LjyMD6aSnmmEQXInqLRoBTL3gLYqjabIOiMs83T/O9fk3a9VdK8ETQH3LlMWYUCv87iY2pp18EZrNYGPWRl5Ys+t3c68veXt+ZtBShVtQtl2pyWpP1s+I+1jrOBH7+SkXMl/khocmDPXXPMXiMi0Ty+DibRuR4d6WmDzdQ6ZzieCcI6gyN1mP9YNJY5tpBzK5SMnZSpOgJsBTKFhRNgthkgCUUWg69aERnwARXAYC1bWMl/vbCyWm7WGDqXiEtx6XD8zeDhAvJj93Q1ztaoVhxohXvJ6jNr9ROqeckDGxpr8j1rc48nudtCB001kRJgWpwBffJa4vT+qR6na+H3uhdVTHmVBfZzBe56pfJN9G4b5lgN/p4S0pz2LQYQDMysZwzpG9giS2GQSHpJYQdllRMBakdr+0Z8IyqG5Qp8yO/5xBVZXQC4jG9tsEyF91RWIKmGL8IDBWCmdJz+Ciq7AFdTwz1orVvwC9NfIuyl08DPx0VUTG5yWW2xsYPgNlqBqgGChm4R3vNq4aC8UBaj6li0jGDXgg5BE3h7/tgZs20ETyVi50mmsdtTOIZmQ8EqDrUz/6VZi957fyycitT5E8aBJwBV0HwpvMlEWcM+jbEmLWxMyxQ0DNS933AtLLzQedwmDWUSNdVnzcGa3jfUJ0AvEgTRnmWOjaOr1huUMVDjBpsVwtjy0KdURIlmJ+1JyEAEjlkmGjTbRpb4SnGxdPIn3MNp5TllDtxgwSyAyFVIXQw5Pa4RYSsQxaB3vmabAan2sGzUlRVP77rZO2Iq6d8L5IDHWwO2bIIBT8oGT/VQBSJfC7vNbxi05teeTOmtg6DeyXdnDwm+4tgFZFOldHrPe1qUvXVuVG8b8/XJYHmatKIe/9VjBbKb70Wh19Rc+k5kZjY7/Droxmpt4FK/h7QDG9DbVQ14mrcFSCJ/zHW6cJz1aDQGfDK7lLG7y/O2zRxTSd3FogFWMkS76kUZkktmy0MTLn3/BbFBAbJSP7flJfiXflDLsM8vHF6sREfcZyOIzBsygXTJD5S20RyERJoqdmK40wfuH4E8aX6mTXbOfoD2/vsXXJNLq67Hb/ox62KGM937ChQ9o1xpR4A74y62daWUqDP4O1lV7tSMVSi5E9Pemm1oM5dHJugHA2MMv9ZgZrFM2XxnxaGvFtVhDMqENEotyVMUG3sOBXWY+72y+Z7R0dPf6tDDg625uMef2j4CFyIX/ZbXZ/9Crcegh8iPO+G7KVuZUEhFDE1VNI3/DRykPQMzdBl02zr7vprPYm6hcN6C+hoDxETeqaIIL1GV145naqYAkGy9ZOAZyqqnBqpxQw23lM8jNmE1SmjYfHMDjnWDEPHoFYA6btzHZhdOD6QHhK1NK8gIqa4BeZwQbU97hkaMDhb1lJgRizherN4jpW782YJdlInMGUMBu1fS+HEYolLmlm/PobyghNO0eXeqDUtnHVrv7R9VCT3CjIaHPNFT8mFsmk2AfTrhNAf77jfI01rxfu4HkXCd2YDJiDK+MlMj2Vvtaf/G1ueh382fFEe4akWNlEtJcvCUGvP4V8N7s84SFzVCLxIc1Geqz4ooZixd9zm+x1lsfQiBN+H9TqGLVO7qdCWkn8Y4yLTQ5mdx9Q1XBiP923VDZ/kPhPoMaSMZVBYm5xLHKKNxGZAzXnv+EdUtTriy8D5xtuF5jCVjGghHHVzTHOZqv6oEWYZfvTLiu4upND0w7TmEesqkZtz2YOhwy/8aTRYekPD8HssyFZxv9CmRo3UIXUflR/PeLTswVEYLszCQBM2nVHRfBuGBJmItrByFHipwugHLYsYRMYBFixKvH9u6SsiYU5EFshh+FUqgcHehEAvUnBZJEQO0e23QtCaC05zw+ulCjetM01WdlCYMrS3nvnNtPxi+P5ANAcQi3SlZwrL1sBCnaI9fxbUVnlur8ffGpPUYNRzvfNYej4nHWdQae47qK2TrPFMufI/12IqzmD1QPBDvRgEv63e75aVMYxHOMJzHaGuE70Xq2rJSjMx/3tfxSiJeype4Lj5xNCuQwhwA9cVvNVOoa6a/FGwYMaTXxCV0O9s1dcKgU9+nmECNu5lYS0i3j5v0i1/HzbYQSsqpm3WBjhOfw8vLBI3R5P8oeigzVUTy1URyiTDvK5KTdCgM5ZDv+5EJg3d+vfM7H82y68M80nA4dvPcPzBQNUn7cd3wtjk16aLF8oZ5HaGFohqNBL5VIGOxmyOTVPE5zOXtSaAtg1FpSmo++7xp12ru3Zru8pj7s8bIB/sHuobqMykD+2M35xcnB1bC5O6Q2b4hhKu4q7nqJoBpHSnfLkAKArR2fA5Vnd90zgFgIIZA1sVePKODrtvO76NGshX5hLYRpvQyJON1JNYZHi0LnqBiya5uMWaN9UL4uCClr8b/5ks/z5oDfCk0VkWqRd+5GwWwxojIW228iXHf+lAGlv1mMYMzZObkIi18/8hakdHFVjoQU1bHkpv7z6bhHY0F1Z7mg6J/G6aA3StMoEFE69P6jeNx9Tj75EQLJBIerKisfkFCFMYrGRCgqdyz9PckHTUQrToDLhjJaNBQTLdPwBr9uAd+McUzUZRQjgNLc/yOHvI9LJ3tWsZiTPGS8ej9kC6SawpZfa9cbuz/12F2wGFZ8rRSkqCRmW2BFol5dYfaKS+FOUvWHFPAeRSq36IA+jqAGSxDa98KmrnCVahN+oU4ssyL3hvWsXVZIQR9XjXn90fzXqk92t08c6MaJ8puOoe4UrOax0nswfRoGhV8ZD8Z6dHNLekrLc7wBxEvDO/MX2o73YJ/117jMD86fV464cGFTdZZSmMboS2DzdcW8xHRSCZgGkZRBZAaYRL9ABMMF/KstItklhqZbFzRiB7klM3hshrzSZQT7djS6mLO5ToU2YikiK5fFcCREmc0lvcG7akzuGWjM4hPF0QmdOG23geoQcn9h0rGQyDpXZb7pPWXMW8+R9KolmBt21gZq/tPinIUaRO6SUxr14SGfRf9V0WDmWa8HnieRAP2udAxtlHEB2SBvRzyDBUzaclIOLe/R1oc4Ic6L6wkFp8PUnXJFasdcrHDWwqTncuYygx9DHvHhZ3WzL/0oS2NhzAeg1xn6akqyFQrpt84Q+KhiaGrPi8ofTkqbLWPDfEz41kaXvpBunmnJIMYgb2j/gL2ERiBt2+fNhrzkgf0OuUZM27MrG0LB6M0gxnKBCSbDYEI8TooTaJwg0eduWzHa0YtlVP+i2tuJNcaDLQ2xVTidsKCGzieBN+na1TtanFO2/yznLlYWq77uDDXdlKbkaRPFiB3tjUlJxFB5gcbl1QQl0IlRWW0zIxkmdyjSmCjwAk74f7TsoN0+NFEwt3k4NBN38yaS2b3HET07MkgI7chtS1s3gaPzBUeTXWcy+UAvxaqC6Wx50LkSXAPU+14dh4QOR34Wk2/4RR3jalC7c2SwRiPCDPf1WTe+Dl0+Q98h9eZyWdXwYgoKN4U0MBsmEfuLKqm20+PWEI8scPEXtP4TPeOF2woqAT9ieaPdF6jWWKUNzbXoZyTsI4dW20dT/gee/5A+QCldRfYU/X9PoTLrpfTYY9jxWR1T9TSj61AA+QuDqXKaTjMj4nwYR0Xi7Q8YwBarW8k2Gu0f4nmELdF+g8ilC4VYGqSh2B0hhqoaqeqVDVlvCcKDex0EQS0kQRci+a+Mher82YqbN4S9ReVXo24RyflYwIlHyhNC+KfkVqiEuU9rjBbeQ2f7h0zu7Rmv818+Y7DIbWGWfw8wAy9R1eq4GNHQNh0EnMBPe2NqDR7gH94fKNKujcq62R8U4E925TGcTXyK+dNOs/8nzRI+zQB1mg+qXZONy/DhsJvFXodW/BTDAUo1fTmnSPtLe6JnzO2HImXSeyc7YiJVQcIombhCxVmBWt6qEi0UauKRQ4EoE+eHjpL+MeuhvGMyJ5xI9aYVHNV/HOxIbpMwUJcMS8Kf5iQhixfT8ExPzwdlqIa2Uihcg1ff5EjQO2/nQ+0PG5No3hY/U6sUI7E4Rv/qg20aTxFjFmaueQB2rbj86UFRn8yC71NQm8OVJpny0GLG6ogPSu5GDXURnFyFttOIW5j9a3WlzKogpG/tr0+QueTGSMGoXElKsFC1/53nMJUG/YYQvWlb8USlSQN76UgwR6BD7S62tVapg0dQ4tTTEvd444h0SPtR1DqOb2i0M9DtA5KrimdU25xjpeiOyRgktUTNWz4iZkNq7TDyz3X5O1yFKk/CMgNGHOEX/ZuU1P3HYIa+0CqkeLebebKXnnFIhhUztiq9G6LA4WzCTj7sLCp7UVrDUCzPAL8pbmZRgkwvW4KmuK9M9LQZri+J6Mmlh5Rdl38AyqCd1g6MvU44QIAQca6XN1j3yoS45+71biHNlw1IRXACCQmfnZR/vljTAIL/xnoS+HuykP+ca4Z4Bda145hbJXm8vW6EboKDKUSiQY+iTNWZQQAm/V2SAobnOs8nMtZ9RMLXYDCFnxmdTA9bpKB8ZAu8HtIaf3PcqRPoI7XaCAQmBd61vGCfn8pfPNn+PNbSQCcFJQqE+RbQO4ETJQbwbmR3mmXBjhRIqJtuRzlZyktwuE0YLp9/K++GxwvjawweNNModxXeNqp1J1LGlLMCAiH0oorFn1Tu/1Pdp5PE+NuQW3jkk3Ays/uqza5OFjWYAs6DLLNKHjzm9MY2157sLiC2FIKjy/iVPoe9UiEZ+Rce9Gjf6R+9Pz/W2TXhgHQkfJe44qgN1cJ9vu93wRF5sr7C9W3/C/0blcLV5jF4kObRVOzNmy0VqlYnANdHBEx3Mpadgl3zRfo+IGE+aP0mvCt7qOXHG741mhaR5YTWvdgy+9qs7TxbUSu9v/lLuiK7LYJP4C0ryKsJ8XMUmJSSPqxVfxCr5Zzv8JDpq8pMCh1CMjY7/N8h6Mv56DEqKvreE+jo5Gway/irt3DFjHnwwdMG+ArP00V/jeCdFb3g2LJMpuAPPtVYohrNDuGu2nPpFOHI9Di+93FxIitVdQTaBozfj21+PfIUckvBVESF/lVPbknfvwdBRRn4UTStAg9JsnNHwqQu+rfExOHHbjxGZBmOANeekRNN7fvz2o96pBFNFqwXgijMzyFKCZSCGo9LQ3OTBOQscqguYXvs+7lLdro1K54rhcFyLgZn5OcLxnhnUyXdBcTJ2kIbnYmIJ9jUgvBnQbcSOoCuJBrGVhbRLtw3NMcEkzwR9LPFNTYVZ7r0TxVn7p7xzjXBrMKQjq6kCds3SQ2KO2B/5jsu0Tmkyaol6yRsfvH6jnkc4mJMheqUwtR2T78QhU5UBZcXIqJngECxmXvkqELfwwaIECDniZKqHhofHj9/72Mcw521AyMFqtDu9aFgXJaePs4Vs99NPEoo8jNB0vgjeiXOmKEu7oZF+tnMTZ66ZZLsmhQXaZlDxAGr8XMmRXTtVHJYgDKpsbp9GPLJ55QURx+sC9McfsX2mxc4XESbbwlRL2Pv5AebgyAXFGZOarzrNQCXIWIjLQQEphLCSiE20dqFJAhnuUJFzxgUCUDR7HdWx8GiLzRGkeUVY6xUVuTL7qvbxdQkiN0P+yDhZ5zb69ABNdptD6wA2LmfdxE7HoWhfS3JCcFTjOqNTpbKOMRLt1g5YWGXpo1KUgae2dtcgtMFU2DIoAf1iizkCGXSctstVoD/zpq9Pxgr2QLxnIhd4AiNgpeguCjAAPiaHwXBxciNmKZWn0VPrTxiFcnEywfcOsAMp3sB5HhnhBbuwIJQXg3YslUY7/Bh/b1aT+lwcNj+u8KsZusi0XXd6ul/3/dRUrVbJ093+mO9SgYfT7pK0EjxPQgg/ZU0x3+rwbaZjIjg7K/J9xWHK8nWwhWWW28PW6f+HbwO8GC2sx5bEZlCFpT6qw1fhVtBpn7aeN7mitb994yOJ/Ot3RSpngtLTp5f3V75FVhrkj3OHxyOZgXx43qiGFTeBHQwbZbN+YLNxXdgFlmZoL/oryv+9Qgdl90dyqbWURNe05Nd5mVx77GY+yMQA7IoDUpD716mi+nugoGu/Po0xZbf35i1l502xbPvfUquDYW/oO8xfkSjBB8l5XxhOQKSodZ6R8ySyzu3HvNwVh1zP6VTwyksPUaAMO/LISe3gqpMthTGqPMJuyd/Fjv4Xbxw8yYPSYMF9O9Urdu7mk4by3C9KJHHTZj1nYVvb5yKv/v3FrPLhKtx70Ea68ZApzj7Lkq1ENsJHAsMMjiW8R3YaD1BeI4Ab4TGRyhzW86TWJbh/lsh80TiilnT3z6ULxVvAAm3TXtXb4Ach7tXd/JUQ9+Tt/fAbM7vWnx3lSnyKESWhsx7mFEp98zFr6gtATuU+zo5ueSKlphxUIUS+mNyqZ5OjUMPW8oX5KEYEKRVAGnt7JCXsQWFBZF6UrLotvIC/Rn946++spq1qVz6tQ0HXuzynym24ZCNJxo9QBNIhEYibDw747jvXRP0DpI+Gcgo7f4EW6rDXbQjKaOOxa0i8hU+/Ol4OKdNltl5ufZLwCwAvEf4KeqotOUaM6dOV7TM5U4BfH0AMu7XVxXSp2QF2YNgQi231EnLoRwRoAMZqMJXUZMdjzmofx+2HWuN6pqYO+C252NLgQLTxG9WAwLh0NwahqvPL1xx+JfpWZhyrUQuqxxAF+O+Zusrambo+QwzTUMKnpTFUEDLdYnEKnRPImlwy10tcfFF0vaG1C0SpIbSR4MNpoqwytpFiJW8pzHBmtK7ce8S39JtZLXIrnsZ1iblVEq47+mM0E4y/T6cUO+uOP63/zMi9kHjCIyL7I5PTcI8pgMnY09rKQUjpoWmc3IEGiE2M2Hh2g4bROFWWWlxewxta3LHJyP9CiDkPF0GGOELG0RzbdzqSCkFQsae9O/vnN3OsgpSUCpcbou3gLEQDo+97jEKL/D+N6nSYojfN9J6RQ7EkUqBSRAMsBEvE+tKzzjtMcAX6aLfbhYnGcfvqvkFSh7Nzqua97rjTvTFTGH3NOVUyG40Syv5bExoJTCo97/9cRYLAGBYbg1Es6c9lAWX15IcaIl+EWgB/vTCrLL5/awG9H3qF9CVVZssiVfi6dMaevmdC7AKlYDEKJjk70X/LJaDAUYX3Ti+Gqrawris8lcvle/ciWkoip0ObAawXNyFkzp5fvJ6p4TJIvNmMwkZX6iYrJ8jTMWKcG3V+j/vkokP/RZKf4S+E8hoi5zzhyP7BfYCzsKjhN2wdJtFEEt9ULAJoBIlj1CFncwjPmxan5aW3VU2w674ZtpGLCBzQW+IkD6jCgOCLG3IXcWfk4a4FDB4f0TqQ2+nGbM100sL3+Xx9ahzdr7fHFgILI7yBVq+kXVFnU69CVvwMWd7ZnwsEOybELIenerDk+qMW7uoMdHNtaUpN6ARGk3XhA5JLjkWFAWvys01VSCuhssdSRu3Kwken7zstOSELpAtXO0svEuivjzhmhakB2AQariiyQSQ5VJxNM0zb6x56sKE4VBOCZLwewzKk8XdtZ4eaW2D45K9Sg/JojmVC7JqPJCRFQjo5V6HQPWEml3P1RPParQTxO36z0JUOaKOSNOAxtVoJwtT/pkjgyyNTjYZoHtRB65Z3okxyrcmqxpm5VVLiZpNoBqQtpx9ME/g1o6fZiLr0CRUVXHrIE68RZ+c7LhXSV1brNmh1y0GsDnxLNaakfH1XSnOWyRVTaJ8Jywe3bb81uAmJHXzULeLHzmrSQXHjz+pEJSp9U0uOwFpGySEia7eN8Hgm92F0xf3JZncoK73tvP0oFqtfz2wg4IU+EnW1Zn28x2ZEzv8ETd9bJyfunYmJmiE0rN71YG9RV2EdbA9I8JwnsaekOyo6aGp68Uo33dciHg+7o6l/f1PvkyPjlwu9YaTMAR3ux8qTtpre4I6JkMujz0OZ8VMYwKflTLSSLQw+tSEPCnmhdt1M7nBMLCbk++KtdpQ+84nhWxeOqKwg9/wfuQLrai4SgqmXp6muwHUu7SyBD8v7ftOOsusV1rkkDJ+Y4ntbcZNRzQXufXn9Dg6r6H/b0KKWVJoNAewq68y8X59XirNa513JSXGIBWNxAem2M3FzJ+wNUs8/UpEv0CZ3qtXD2PhAMFYNFmJ6LFSBn5iB9HVe7UuzT2X+c4vOwI6k4veDFqzpPpfoeG1FLwUDGS4qyAr4eI1w2NyXY7rxd9BpbP6Gv1vFi4g5V4OJG8TAJMzS+rPSheNyOb/NcHyE1ujYM9f5z/ihJaAse5gx9rDjvq+hKZuBCGiOs+pTgIuaU/dkaXPPhAllN0n9Gz11JPBriSfriQWbCWRUgtkMO836DFnsD/N1GlDpTe6c6Z0LJqrcbb/LzUQmHvKQm9LtzPvdZpf7Yd5wTkvjAhqpgBtyS4p0KARSJJ95nAA1OABlZy4zSM9jWVf08RnpEZghYPBSfqdTMZvnfgHQ5SCsnzeLHmB1kdT08PDYadp8PABYIXYBM+FoPP09DQ5AnEa3Vi5dTVb03GzscwukXyRCx7kj+dy9A4f+mcjx+uToJ2j8Y4Hn00VIcyEWet6tIGiOL1AanYsSUg+hbUzYpOBrDSKuaY0zaiFFarbAPRRrALGyEVJs45799rLrBASbIhAkrQSajEk5R+IwNhjLYgGGDbm4yvVTncs/8zYIbNxop3I4iPqop3OTQRExb2H3Dtz7Ttgd4Wastys4MISWRbjLPas9IwUaaaVGysuvSWQ9n8pa/25ESYphidk+DD3a9lCMB+xAcY/Xt+UQ0TeZGEArxOkkuUJUPanNgUxIwQYHx+6q5qYMyLXLTnbb8DHcxxcFvtSmEIWKTkRBN2EaAOzHWDdmvJ7UKMDWo9VM3YbwfxX2ak5GBtlqs5GPlzZyW/QgrAAvfg/JFufxY0Z8W1kUY3yAFQgUVAWwI55nqiSCmGgRlt2uMXkKWnidJ/xY8qCr6W8mV4mp5k4N3OmIOCPFa9q4pb0vrjxonTdpxbVv2Pfz7Hp3jjbR9WYUtThQTcvdya7yH1Wevrh3zoFNVMndabs92skKYlS1jTXLYW6Rr3VFZ7MAgNIEAeUWR+Ztem2x/LXtzRirwJxOYgwWiteBkCcRKEo8Zy/bpVxSJk2dUDfx0S2idNRoJdNZ8RcqvQDSufWMyxXuPSrvx+qVxuQr0lCJ2cTFv0qpjVYmiJ1oxsXs7xOFmoK4jHLvqmHdFL3jBA3fCBWqYH58zi86OxmDmsK1qYqHDAmTY8PnCcMl/YhiNyptULi15F71ckot6HZGyNY4PG5/y9390hG6IuYhIbEnmx3UEpmKliUS4EIlQVuRPpkCsaGMMZ48KjYB9MzLCQdpq1W3pI+H8bB4jMyQ9BUIMHq/CILVosaB2U+47JlD40xaP6LAJ44FNTU1Q2YBmSeBa10tK37utM4w+0a9vfIww1ZbDD9agFs2R62TjAXndJ9bbwG2c4S7ZmP7Dq0JSsOoY+OiMo3J7j/ljZnpkLZftJJtp23zODKDWydlVun3S9P8TcjL5KtjNkGC1b7lnHDyeILEk2FE9UE6hNChoObX5pE87AGXYXt8W9Rsm0TOoeMxsXczTBdy2nvPk8EuPAP0pAo/GRrAorde9cH7eoJOYaj2nWLHrRF9UHL+SUENVwOmLqii8ecjIYQ7M/M147pElSOw69E1MJ+jP8VfIkoXOCh8qCfUFzwm36AiMgIqNE0f7+zhduInSvGCzlk3LVIlfPrkSRRyz1qoubNPuYHa0rzZ6dJFfc3h7C9liqqOr5VSq/EiQoFh3SBWLu94i8zoBkgG8AnaOh/dykp2QjTytiGG2tSDm/It3JmQOFAsp3shs89pcCSD3QIyffOs94GxTPeBnhyH/CItSyjWbELtS1OCAcQxSYkAWD9PPEXYU3eXOKomEk28lmQlMv2p8/TkCBefoANfvMypFWF67FHn2iuykd55/dG1ygqJhptMAIVEmc8YYrHDjbD9wVshwLUI0t3hrPrRqQ7+Iy0DkXiJ9wUvgl/YGgKIKlo7sZmZt5N6Nk5WiKdoCQmz28YGIapm2BTW/b92BUz6LsCJY3Kl+qBJNs5LDDKgzxdgExHtlcIBKvm6dO3aRCPz396msCTZFELpGDZ05BKSGbVrmX+bdEIEkdUtCRtz1oUaTk56ddUQsX8tenJ3d7CjqgmISOScM777MomPdBuWy2I2s+uyy/MXz+nRXP26skePdf0yK4jcQs6J3w8D14QEn0Pp+Sw3Gtl51CLUU3qzju+eyvfT8ppTRm/robWM0Wx8zI0DUZauGWZc1TTYUSr653HfO824FUUssHmLoQzSaT4ivUY1HQLMOXEfgzbVZI8vJm90WT7L0SDtCTxkHiel6X+T2er0AV4U2/MP7XlDq97hJwYGXN3XX/J7SctCStfOpnN7wIYP4KCxTKoKYqHjqavBm9zAxB5SGCsa+AM2A3gfDNEu+vL/UKm9C1JcjbtdZADxczi/sT5MiZR2jHBpG/aMo1G53mEuvi9gJfU8yyW9uqex9PyKCxxTmneqvBVK9VNNqYqWpObZG/jGQwhIF3ufHE2L7qJ7tt01ShrL2UUBRpKcLZnmZk2ID0dd3dQL63cvr47i3/lMbJOo4mFxsU6LJwD1wSAXfDYf4/mE0CD/XL0zmsvCB7sHkw3f6xNpqx9KJKdriOm7AaK0zGg2jUgUPTpiuWYE4vUL/yTLt1+h1aScUqJHIyihFHXFOtofu5u6Y2x6bvv0p95EKcl5kEaWvvBFhodRB+2XhqHVh0D0WT0kXVOHUBkFn64FEq7AtO7U55hYiDp1AJ6YAov03b94ahO8aHeBTDTkB7h00p3cg3YEuHwMA7NDOeGrljermifV5NBpjVz7tsGOJOjnjddbynX1Vz15rUi4oYqajsMpwKR+je/cxkTX7IdFGqrO1nXvI2Og5vGxIprF075uJTcWvDHNjb5oUSrTJ7sM5dV2b6FUHMMFbLziIWDSE3gqmCXsaxcEczHRZLaoEj1WuFiAzFuVbrFCJv7kaQJuBxpWDuCv09jFfp8JYI+/JISxcDe7ObYKh+G3jHNRQFIDnQv5+gQmjDDFU53foMf/CqlylhjIR+9JvEjMMiuQhrUk7b/aaDO7wOPlLm1GXt3lmFbfwL4zUhLTqvS+URgXGtYmdPxpgFTATKr+g3ye3+E7zrT7Ke3HX+GPClnpQGdulnlmMJorK7hBGhDhpu5NECaoJ3YLemYzvDg8apwpLLoKWd5oOqeQdFzJkz4pXdKPnUh8Xtf4LaAKCz8otR+M9F+QOAXyk0tMj8QWafPGyAaDWou6hMqxXEQasjWSitIleibpnR5LTMv5dL+LDYimlTDKD91FySqfpZ3FZUuC+SzO061WfXe8g7DMnUrNwO3qup+wFGC0Tv/8Jgg2krGomqGj8Up2eFh5dgcg/oZtf3tjj+GmaI1kaJ+o+8HpXEOwgHcF6wD5f8TLN9FBPVHpMbpQ14q/tLrhVMUfvnoCm33+s6irbf15e2QjaPd+tL8TC5MTcowA/NNE/mm5IIC86hoe/EkgapHcuJpfY8sqNGj+NOL4cOrI/PWgGaVU6fmAdtWUTmNjlPBV1Cr9eftkRUnWw+cwQbFajRVNWZHXlHSkNN53EdRaWczTnxY1PVSw3I8N/Tlsy9k7ofzYW9Y3Hw7Yj+RAJWlLyJv2aq4YhWDnfOm/7HOdr37Swku2645il9SH8ISTu7UIi0+J+rlUYoBnfb6LEQqhd2+e5+lD5+TumKagCOJUElHUq8lprhTrS2otSImd+eQlFBbUpe91B/dCHi+H+9M8EGSop/OMvIEIERB2oHKCxzKcp6hNRpZpV9NCQRzpwBrZMtF8EVXSKZRT3RtsibP8DjIlSTu7xMFPd99rroKBe/hNoHJV1qv/jQ/CMwog54c0UJFXU2X01NgsJybtvpWZ+jCAn5bFyw/v6letinHoiRR2Qxs8EfklUqmTelsowJDL3a0g2+iYpz17kYFzCTm/9EQWrUyBLnFbJrdVpSYub1dK3yVg4NCzbieO1kPBf55maANRPIAqoYKIaReSlQ+5lmEdBIsGdrNb4FxmLmKqlxO30rJMlfJiLg6EMPvHTTF4cBxeH8cRXfr3mwBgyHVnvj+BaBmS8IiXl9ChkxVwOAKkDpZSJvdgeXL229061VOBjG8xBjKMRwwyZ2qAE0cbbRTVgVv8zjrGGWGNzoJSV504M34ZsHyLME+YzbAdJ3bbAI5Av85NIEvjbbhPBN72SdZhuv32PAqUrWXBUndkUODW5pZkkXld5tW2W8D07W99y8CfJ0VYHdiOq1J1NwONPeU5ioKd6mN9WneIxDeqeZKq3i8g4SXzQgaBxpsE3Rg8pgdUMZ5FfDNqia8Rr6p2Hy6Nz0fV7dYafVbuRsVezGnDJSn4Fb2o3WC9KI4dTRi9FHoEqWzw4dJ+Mi/ci6X/jNdOi5uGwYctNisZ4OivdgYpNHv2J6ewI6df59WJqEuKttvpJYjVBFKdcFYSb7vyWS6WMV5m5cMM5qp4shfRnNW2KI+QGXsemltl8YrPeCVXtCEK41E71iZnxGpFFbuMVnVddSSwDhYiOFJfwN0vkhUMN0eaUKTagua7tVhWn1Zk79Xk7d5hMf21zJFyqGDvyrm8wim0ek6JHFZP8KmHaIbmTngXA0Uy97jdH92sWzl2mS7QU/5yPzIVf5hh0DOtgBgw99ycE3qGKqvEBbjM1YcdB35NHAnK5z3+Ynp3ylVv5WACqgD08oJ/VQze6cKuRlqhJ7+oKtUuGozD+CtPN3lhBc7azJk7AG+m42iBdLvM1NP9NRUEo6/zUh1/oJwL6qU1owve9hrfkTIiKoZ8zJjGSOVNH693UmPiO9Q0GucOVvQsmruufPj6CTARNOQuBaeUIpSFlrI190D3Sq5azdAHg0veYkhJxQZFY8RD+tU93geubAItZEMiEMB0K9mSIjLPt+00gywT0B/zUeODGDp5IKTHBmgUntCz/k7v8L/NGoJd/QhMVUtlTyH/0gINIon0bYHITOk1wOWFo4p6xFdHmFBaXzNrUoWra6BvlKf9Tti8YwyvGNvZrgKNeU07KdmuwqAJlIm3+Vj+G1Zhh9+29EATcaslaVato5rs/QekhzZ0m1RNj4i+L810F9a8qXnvGsm7ObkN16S+p885C414hZi3N+ARYtYev9Hth9m43rxjC0ThiLLeiXI6nBJx6r+bCH7QrLNjmirMVA6Be4YLAHSRyNrbwcGfRw9jqW6Sq6KPqx7WEDaLc/IoEh2kF4MyRX885a7JHu/Y7HnBVOMqghrlUEFf7n4WORoqwGd474HTCRUBb2J+y1bK8lBbKaOBbU3Tnc3FZVW7tkbMruL3WVSt7ssCZBED7fvJKupwtnuJlcoBdGat2CXOZbMCKvMSyf2JrFP1l9LLSNo5wU9aAlFUxtkf6+9cgCxRvxt6Loj7hGUSnXI+pXztgFpPBjoAzhEdFgTfc8M35Y/TV6f3H7aflr5vKmaXR1AitB9jsI4xd5R4c6afo6S5nwY15R5KFDYRL0QKkAEoNEOkAudEFvxYF2pyoRk6NPmIX+Jx3xZYtJ3Bx4tTZ1KcoPVq63J4YwudPrb7GBNVaNsY013JNInB2yj847QnPgK/qV1jfsq9+uwK7qqN7Z7m1OR4otumBCBDSGXOApqtbvIirNhfmGYYU/Tu+R643sHEWFVFjK6s9CU8jL2F9NbbGqiliWRv7igbHf1TIcLXBPbxOLMFNBO+/sG9NKTft2YlofV7y9/q4BpQ9b141R1Skgzr/XhyZ6x/rDujnR81sGjsZHT1sd47Jn2CwR46lOAJPNn8dgbRdLmNnjbydOcBaY5EaT3V8GBbkeIeYSTgCMuKfDcO5n9zmaSFFGDn0MkteF69ASsvJkqWH3fS+pIOQeE17Y9BQvHa0FQcooCtvnE8HGGBbOTz/BFy/PWisZy4NIkgpkfnLdDW7YNkDKqKIFMt+zsw+ceC0xvVoIP7oMXRYYjtcAENlFeQJNiOVKuzlLZNoaBXRIShraz05D03S4KMZjQx7V7B8H0eN3WfGObcDVTq32Cx5Wk2934FqEumvA1BD+HevhLNS4umhQe1V5255c49QIhf1bYBpQc4EtXGEfHS+8lmFe1sdOV93Pz5A2slOAtc4JJxnSij5rQGMB+2348IreJeDRtN/z601yjvUiy7NtELP2Nwcg6RWxpOUKE2LyfmC2kVMDUDZ/2P9kc8bfIhe/73h62hvTo6G6n2cNAFbFpP/qAuX1T3TlUCz7wBKY7OWX9D8IGE4bTla3WKMUgaFMMPxcVB693nlGw5gLuWf9sMsbz1PJXsM8UwR+KcALmkRscze9JtJlD0/24AoHw5XMSgt0FIx+y0VFIrH+heqy1NareoGKqaI1wLkQW9AX6zkYBlDBSrvL/SnHGWwSKwso5Vjb00pczcZxfTroznIsT7iKJ7hzhBtFY/w62kq2PztmXKJl/WexGnmH8qTqv/0eY+xDac0pxYqcjrqFtCKjAAescH1+zvZuWESYa4QR9EqTNDWkc6eDPuDP4duzPxrXz23F6o7pJA8X/Gigt+yvaFDb1pPPfOXxbe5dpIzIxublJqoa+ER42IL4WdllXb4oCpXqbc4ZWFsqj/QPpmeuGE7Q8cbSgNASrlWxNICGpeW9Pp+wy0flDL+VKtN4yWwrn2+Lfh9ocxgY/PxcxzpO1emHSI5ksh0RW821+ZKUYYCwNU0Vs5cd2GhzP3F2ATCCdhy22KWm2Vul5zg8oI9EKQbYXaJtkaxqI9U1CTFLbmjzIUMdVkxtij25vbk2zvhK2qpOU69iYO4KvlasIRlkTqbAcIvzfrvLTYhYWbCjy0fZL/PsIRZX6oBzxUxtShnzVVSprbcKrQb2cwlbjmjYcwHekHMVdLMd76ylkBCVkGEISPNnZoGRhyJAoSxp/wDPAb2dRTzW39+Hsq4UMVYp9PINTT/c58B9ET0SURRYjVsdK71LdZxGuY18KpkH1unWV5J1GZzTJTpHraAn2HPZMwaF7+8LfhSHG+1l94jlDNOEMKm4eDofQilK6exktNIACUWBOYF/IWaw90QaGm3PQQVDD5enRCmTuN2zm3xFhH90x4uj9SHg/HMkTN7tTEAZ4genxEzWj1aFJ7fJidM+gw6wPygQnJPTCYY4cdYwBAqyoeHKXjVBqzZ/fBXZG735U1cU6i4uqKs4MA/RmUWSVvcjlnAb8MNztpdaEDzPHs+6ZNIa6GjXZBr3biH5hoo79Bb/WRi4ojN0ZhBDol0iL6ujYoBOAzhjnTuT/NSsQa3bX8XU64AV/VIrgOQgwLDwkrxOHy2H8MYZ75iUsDUxhBX08Raiz4kBUjHlRFXPrIPdDQddfixDgnhZWDsBNz4Kn7FzameCULDleRnQ+JVks80MeC5SHa9gRy9fmYTCAllqyRtSfHsuqjw0KeWrqSzF13m2k67t483+uVfMR52UTKy487m5VDp6nrbfPdpuYqG8L8N60+JLgB+iOpR1aVFBROA7CH3xnj3BXLiKVvz2+xIDI1tBJgFBpOPaTzavlAoRmB5QahWe5cHDl/8XtPx2pKFHedVkImOaoSHvOLSO/UgQPxIreD5xKC0eUE+qmnILSfA130GD9G/4hTebxlGkgw4Sja+XPsz0whv6tNVyOq/iBM99s+WjPblIlGotnSex7abVPLJZfgqAjJCaF7KVDVyETvERFwNgXh+S5MlOD8DpAyOUFcBgKT18Iz8gAp9tvahe0HUwqZGuUFzqGpO0tvu847FC9onPpGUFcP1uWJQArF47HsBOggPRhSQ5wJruCCzETbfRka0cFn38WdKnGnKr6B8aiFYZxOGwhMvqfA0gdbIxQ5yY62fFx6r0Vkq6X+dn2/D13kPXSFnkj4uyVK8X5sa1sc6mzS9956XxF1//1QfAIfjgrXk4NMOXWheoE+IzzS+aAazPCrg9+N8K96eJA+r7bM2aU7Elqm0Uq4bCtj48vVxknlUGmYOYzNaM4NwIoEVoRJhuw1ySSlOFK7jv9bnOUtaLFv+5C4U9wGmVfwnptF6+PFsoB7qmzatSVW04G9fSElCSboN574pZQOD95zl886P9NX0P5icrSlhpXuixNqhbwJ+llec9w3oRQvWlYW1ueNoNEudutMjTnUPLBHWSRW0AZKtvqxzpFOuwz7h2UIUFNXh/xFvsXF6hllWQ0yOb3KrjH2v7/EM0+71Db0/hIsTNZyvORN34E9LPZ2cJ3ApqAxjZPcO0Ju8KIw+pz9YMFhQcAvQbUToeSb5zbcRHWBO307JlowdGS3xn7qXdOJjr39Xfgwc2EHIivysuFdEc4YQZh/ZYudMzlC3c2X8sUOuBHNDzLUMYowxFD9cWZE9ZZRzDm9J1dnEim5q98bnhbzMN5U0+3E7Suc6jFeQR1p6xIm8CO3fg2CPdU+vilqXAr9WqY6qWA9Kn7xHMBfI7mBPv195rnNOiDGLRm79Nt1rhLNX0SraqedYAuNyxwCIE98Xlc92YjLi4ocOZMTaOQilnd+nKA4goddhKFwQXrYDe9fTEHi2r4Y8EJzD8uR+7QTWO6kD7oTRNNDVP9PhZ1o7J1HO9z5rBzjMGW/NYtdHH0LT0mgZwfK/w/RcdyXrE1zxmNc/TtNlBPVb2hgLVyOmSoOK0vm0GRqIxP1bfOHv8O/m1ikalodJB1WWLsCsrSQDrYHJCoQ6qaWM39SdCIhVsnCI17/6OMXvZpH1Y0apcc/4o3aiRMKkaFyROyr1T7etP9O4cOMZYz5nDckEgohSWSJsupicGMDEhleJOvAeJVP2hUi6+fTMUvcKi6jYTGfhDGfPHpadLz+cGmdeZmAzi+E7wHfN4PY14ZDIcFP4octV7TWzzIRkamigJ0/YZCSj3DPjJhjXoxSmtO27vhFs799GN4BLikFdILxeefR0txvSyQpCg7fIrBNBPuVCvkGW33OlTgRHkWJ/LGYL+vkswi4zCvPQ+MDbPqiU21vBTf8UjJcGyj8xU9G6wIWtSNnArPjaJ5TvePWKZDuze0bHVRpESN5LngpeuSdS2dnWhOWpSv6TbJrhdsZxdsHvjKxS1SnJdNIe9HP+FMXFdnOFnULfA8/I/m86NLzDUpfuhPEOIcKGib5eUdvVKy9zZTMp7iG3x0KZ/A3jHOlGAUbD/dcURuc0KynmJQ91tnk9JCNJnq3Hc8vl7xwOTJ3EBGItls4W0SbOdKwQ4tVFpQfcBpCXFJCixrZy2AsKRSvrw+wIxeQkunUy499sR82EheW3UM4jugMvGiiiCLr1TXwMdUdSaUdIVrkwgf/a0/Y6DtLBARqB/E7X+Fk12W4QTXq4Tk2QASDu2yEtY9BlqcQ3U5z9JBWBiRSDywRdecwUapq4+EBFFMyus1nzocxyU3Lr2fQhb4UyIAzZKFbhBdY/iolFwK3bA+8evD5Mp2ixjlntPAu003du003d";
//	
//	
//}