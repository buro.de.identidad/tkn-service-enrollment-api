package com.teknei.bid.service.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "${tkn.feign.biometric-name}")
public interface BiometricCaptureClient {

    @RequestMapping(value = "/captureBiometric/capture/init/{biometricId}", method = RequestMethod.GET)
    String initCapture(@PathVariable("biometricId") String biometricId);

    @RequestMapping(value = "/captureBiometric/capture/status-detail/{biometricId}", method = RequestMethod.GET)
    String getDetailFor(@PathVariable("biometricId") String biometricId);

    @RequestMapping(value = "/captureBiometric/capture/init/{biometricId}/{ll}/{lr}/{lm}/{li}/{lt}/{rl}/{rr}/{rm}/{ri}/{rt}/{scanId}/{customerId}", method = RequestMethod.GET)
    String initCaptureDetail(@PathVariable("biometricId") String biometricId, @PathVariable("ll") Boolean ll, @PathVariable("lr") Boolean lr, @PathVariable("lm") Boolean lm, @PathVariable("li") Boolean li, @PathVariable("lt") Boolean lt, @PathVariable("rl") Boolean rl, @PathVariable("rr") Boolean rr, @PathVariable("rm") Boolean rm, @PathVariable("ri") Boolean ri, @PathVariable("rt") Boolean rt, @PathVariable("scanId") String scanId, @PathVariable("customerId") Long customerId);

    @RequestMapping(value = {"/captureBiometric/capture/status/"}, method = RequestMethod.GET)
    String getStatusCapture();

    @RequestMapping(value = {"/captureBiometric/capture/status/{biometricId}"}, method = RequestMethod.GET)
    String getStatusCaptureBiomId(@PathVariable("biometricId") String biometricId);

    @RequestMapping(value = "/captureBiometric/capture/end", method = RequestMethod.GET)
    String endCapture(HttpServletRequest request);

    @RequestMapping(value = "/captureBiometric/capture/status/slaps/{biometricId}/{idStatus}", method = RequestMethod.POST)
    String setSlapsStatusProcessed(@PathVariable("biometricId") String biometricId, @PathVariable("idStatus") Integer idStatus);

    @RequestMapping(value = {"/captureBiometric/capture/status/slaps"}, method = RequestMethod.GET)
    String getSlapStatusProcessed();

    @RequestMapping(value = {"/captureBiometric/capture/status/slaps/{biometricId}"}, method = RequestMethod.GET)
    String getSlapStatusProcessedBiomId(@PathVariable("biometricId") String biometricId);

    @RequestMapping(value = "/captureBiometric/capture/data", method = RequestMethod.POST, consumes = "application/json")
    String setCaptureSlaps(@RequestBody String jsonSlaps);

    @RequestMapping(value = "/captureBiometric/capture/endError/{biometricId}", method = RequestMethod.POST)
    ResponseEntity<String> endCaptureWithError(@PathVariable("biometricId") String biometricId);

    @RequestMapping(value = "/captureBiometric/capture/getData/slaps/{biometricId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<String> getCaptureSlapsData(@PathVariable("biometricId") String biometricId);
}