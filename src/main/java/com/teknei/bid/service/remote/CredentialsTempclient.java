package com.teknei.bid.service.remote;

import com.teknei.bid.dto.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;

@FeignClient(value = "${tkn.feign.credentials-temp-name}")

public interface CredentialsTempclient {

    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CredentialsValidationResultDTO> delete(@RequestBody CredentialsDeleteRequestDTO requestDTO);

    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CredentialsValidationResultDTO> create(@RequestBody CredentialsSaveRequestDTO requestDTO);

    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CredentialsValidationResultDTO> validate(@RequestBody CredentialsValidationRequestDTO requestDTO);

    @RequestMapping(value = "/credentials/credentials/fingers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CredentialsValidationResultDTO> validateNoFingers(@RequestBody CredentialsValidationFingersRequestDTO requestDTO);

    @RequestMapping(value = "/credentials/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Map<String, Long>> findStatusCred();

    @RequestMapping(value = "/credentials/credentials/{idEstaCred}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<CredentialsTempDTO>> findAllByIdStatus(@PathVariable("idEstaCred") Long idEstaCred);

    @RequestMapping(value = "/credentials/credentials", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<CredentialsTempDTO>> findAll();

    @RequestMapping(value = "/credentials/credentials/modify", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CredentialsValidationResultDTO> updateCredTemp(@RequestBody UpdateTempStatusCredRequestDTO requestDTO);

    @RequestMapping(value = "/credentials/credentials/data", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<PersonalDataCredentialDTO> validateUsername(@RequestBody CredentialsValidationRequestDTO requestDTO);

}