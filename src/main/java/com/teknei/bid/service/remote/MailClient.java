package com.teknei.bid.service.remote;

import com.teknei.bid.dto.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "${tkn.feign.mail-name}")
public interface MailClient {

    @RequestMapping(value = "/mail/OTP", method = RequestMethod.POST)
    ResponseEntity<MailResponseDTO> generateOTP(@RequestBody MailRequestDTOService mailRequestDTO);

    @RequestMapping(value = "/mail/OTP/vc", method = RequestMethod.POST)
    ResponseEntity<MailResponseDTO> generateOTPVideoconference(@RequestBody MailRequestDTOService mailRequestDTO);

    @RequestMapping(value = "/mail/OTP/resend", method = RequestMethod.POST)
    ResponseEntity<MailResponseDTO> reGenerateOTP(@RequestBody MailRequestDTOService mailRequestDTO);

    @RequestMapping(value = "/mail/validateOTP", method = RequestMethod.POST)
    ResponseEntity<String> validateOTP(@RequestBody OTPVerificationDTOService dto);

    @RequestMapping(value = "/mail/contractSigned", method = RequestMethod.POST)
    ResponseEntity<String> sendContract(@RequestBody MailRequestDTOService mailRequestDTO);
    
    //TODO AJGD  -->>{user}/{mail}
    @RequestMapping(value = "/mail/forgetPassword/{user}/{mail}", method = RequestMethod.GET)
    ResponseEntity<String> forgetPassword(@PathVariable("user") String user,@PathVariable("mail") String mail);
    
    @RequestMapping(value = "/mail/forgetPassword/validateOTP", method = RequestMethod.POST)
    ResponseEntity<String> forgetPasswordValidateOTP(@RequestBody OTPVerificationDTO dto);

}