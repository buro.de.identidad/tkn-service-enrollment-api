package com.teknei.bid.util;

import java.util.Iterator;

import org.json.JSONObject;

/**
 * Clase utilitaria para el logeo de json extensos.
 * 
 * @author AJGD
 */
public abstract class LogUtil {

	/**
	 * Obtiene las key del json, recorre y acorta.
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static String logJsonObject(JSONObject jsonObject) {
		try {
			JSONObject jsonObjectToLog = new JSONObject();
			for (Iterator<?> iterator = jsonObject.keys(); iterator.hasNext();) {
				String key = (String) iterator.next();
				Object value = jsonObject.get(key);
				if (value instanceof String) {
					String obj = value.toString().replace("\\", "");
					jsonObjectToLog.put(key, cutString((String) obj));
				}else {
					String obj = value.toString().replace("\\", "");
					jsonObjectToLog.put(key, cutString(value.toString()));
				}
			}
			return "JsonRequest :" + jsonObjectToLog.toString();
		} catch (Exception e) {
			return "JsonRequest : jsonMalformado";
		}
	}
	
	/**
	 * Corta la cadena a 10 en caso de tener mas de 10 caracteres
	 * 
	 * @param min
	 * @return
	 */
	public static String cutString(String min) {
		return (min != null && !min.isEmpty() && min.length() > 9) ? min.substring(0, 10) : min;
	}
}
