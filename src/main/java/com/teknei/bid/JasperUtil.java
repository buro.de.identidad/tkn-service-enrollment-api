//package com.teknei.bid;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.sql.Connection;
//import java.util.Collection;
//import java.util.Map;
//
//import net.sf.jasperreports.engine.JREmptyDataSource;
//import net.sf.jasperreports.engine.JRException;
//import net.sf.jasperreports.engine.JRExporter;
//import net.sf.jasperreports.engine.JRExporterParameter;
//import net.sf.jasperreports.engine.JasperCompileManager;
//import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperFillManager;
//import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
//import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.export.JRCsvExporter;
//import net.sf.jasperreports.engine.export.JRXlsExporter;
//import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
//import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
//import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
//import net.sf.jasperreports.engine.util.JRLoader;
//import org.apache.commons.collections.CollectionUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * @author dhernandez
// */
//public final class JasperUtil {
//
//  private static final Logger logger = LoggerFactory.getLogger( JasperUtil.class );
//
//  private static final String JRXML_SUFFIX = "jrxml";
//
//  private static final String JASPER_SUFFIX = "jasper";
//
//  /**
//   * Constructor. La clase no debe instanciarse.
//   */
//  private JasperUtil() {}
//
//  public static byte[] generatePDF( String jasperPath, Map<String, Object> parameters) {
//    byte[] bytes = null;
//    try {
//      JasperPrint jasperPrint = generateJasper( jasperPath, parameters);
//      bytes = JasperExportManager.exportReportToPdf( jasperPrint );
//    }
//    catch( Exception e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return bytes;
//  }
//
//  public static byte[] generarEXCEL( String jasperPath, Map<String, Object> parameters, Collection<?> data, Connection conn ) {
//    byte[] bytes = null;
//    try {
//      JasperPrint jasperPrint = generateJasper( jasperPath, parameters, data, conn );
//      ByteArrayOutputStream baos = new ByteArrayOutputStream();
//      JRExporter exporter = new JRXlsExporter();
//      exporter.setParameter( JRExporterParameter.JASPER_PRINT, jasperPrint );
//      exporter.setParameter( JRExporterParameter.OUTPUT_STREAM, baos );
//      exporter.setParameter( JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE );
//      exporter.setParameter( JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE );
//      exporter.setParameter( JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE );
//      exporter.setParameter( JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE );
//      exporter.setParameter( JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE );
//      exporter.exportReport();
//      bytes = baos.toByteArray();
//    }
//    catch( JRException e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return bytes;
//  }
//
//  public static JasperPrint generateJasper( String jasperPath, Map<String, Object> parameters) throws JRException, FileNotFoundException {
//    JasperReport jasperReport = null;
////    InputStream inputStream = JasperUtil.class.getClassLoader().getResourceAsStream( jasperPath );
//	File mFile = new File(jasperPath);
//	InputStream inputStream = new FileInputStream(mFile);	
//	
////    if( inputStream == null ) {
////      try {
////        throw new FileNotFoundException( "Path of file: '" + jasperPath + "' not found in the classpath" );
////      }
////      catch( FileNotFoundException e ) {
////        logger.error( e.getMessage(), e );
////      }
////    }
//
//    if( jasperPath.endsWith( JRXML_SUFFIX ) ) {
//      jasperReport = JasperCompileManager.compileReport( inputStream );
//    } else if( jasperPath.endsWith( JASPER_SUFFIX ) ) {
//      jasperReport = (JasperReport) JRLoader.loadObject( inputStream );
//    }
//
//    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
//    
//    try {
//      inputStream.close();
//    }
//    catch( IOException e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return jasperPrint;
//  }
//
//  private static JasperPrint generateJasper( String jasperPath, Map<String, Object> parameters, Collection<?> data, Connection conn ) throws JRException {
//	    JasperReport jasperReport = null;
//	    InputStream inputStream = JasperUtil.class.getClassLoader().getResourceAsStream( jasperPath );
//	    if( inputStream == null ) {
//	      try {
//	        throw new FileNotFoundException( "Path of file: '" + jasperPath + "' not found in the classpath" );
//	      }
//	      catch( FileNotFoundException e ) {
//	        logger.error( e.getMessage(), e );
//	      }
//	    }
//
//	    if( jasperPath.endsWith( JRXML_SUFFIX ) ) {
//	      jasperReport = JasperCompileManager.compileReport( inputStream );
//	    } else if( jasperPath.endsWith( JASPER_SUFFIX ) ) {
//	      jasperReport = (JasperReport) JRLoader.loadObject( inputStream );
//	    }
//
//	    JasperPrint jasperPrint = null;
//	    if( CollectionUtils.isNotEmpty( data ) ) {
//	      JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource( data );
//	      jasperPrint = JasperFillManager.fillReport( jasperReport, parameters, dataSource );
//	    } else {
//	      jasperPrint = JasperFillManager.fillReport( jasperReport, parameters, conn );
//	    }	    
//	    
//	    try {
//	      inputStream.close();
//	    }
//	    catch( IOException e ) {
//	      logger.error( e.getMessage(), e );
//	    }
//	    return jasperPrint;
//	  }
//
//  
//  
//  public static byte[] generarDOCX( String jasperPath, Map<String, Object> parameterMap, Collection<?> data, Connection conn ) {
//    byte[] file = null;
//    try {
//      JasperPrint jasperPrint = generateJasper( jasperPath, parameterMap, data, conn );
//      ByteArrayOutputStream streamPdf = new ByteArrayOutputStream();
//      JRExporter exporter = new JRDocxExporter();
//      exporter.setParameter( JRExporterParameter.JASPER_PRINT, jasperPrint );
//      exporter.setParameter( JRExporterParameter.OUTPUT_STREAM, streamPdf );
//      exporter.exportReport();
//      file = streamPdf.toByteArray();
//    }
//    catch( JRException e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return file;
//  }
//
//  public static byte[] generarPPTX( String jasperPath, Map<String, Object> parameterMap, Collection<?> data, Connection conn ) {
//    byte[] file = null;
//    try {
//      JasperPrint jasperPrint = generateJasper( jasperPath, parameterMap, data, conn );
//      ByteArrayOutputStream streamPdf = new ByteArrayOutputStream();
//      JRExporter exporter = new JRPptxExporter();
//      exporter.setParameter( JRExporterParameter.JASPER_PRINT, jasperPrint );
//      exporter.setParameter( JRExporterParameter.OUTPUT_STREAM, streamPdf );
//      exporter.exportReport();
//      file = streamPdf.toByteArray();
//    }
//    catch( JRException e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return file;
//  }
//
//  public static byte[] generarCSV( String jasperPath, Map<String, Object> parameterMap, Collection<?> data, Connection conn ) {
//    byte[] file = null;
//    try {
//      JasperPrint jasperPrint = generateJasper( jasperPath, parameterMap, data, conn );
//      ByteArrayOutputStream streamPdf = new ByteArrayOutputStream();
//      JRExporter exporter = new JRCsvExporter();
//      exporter.setParameter( JRExporterParameter.JASPER_PRINT, jasperPrint );
//      exporter.setParameter( JRExporterParameter.OUTPUT_STREAM, streamPdf );
//      exporter.exportReport();
//      file = streamPdf.toByteArray();
//    }
//    catch( JRException e ) {
//      logger.error( e.getMessage(), e );
//    }
//    return file;
//  }
//  
//}
