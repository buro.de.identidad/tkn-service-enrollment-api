package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CompareFacialRequestDTO implements Serializable {

    private String b64Face1;
    private String b64Face2;
    private Integer umbral;

}