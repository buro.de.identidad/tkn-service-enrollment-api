package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MailRequestDTO implements Serializable {

    private Long idClient;

}
