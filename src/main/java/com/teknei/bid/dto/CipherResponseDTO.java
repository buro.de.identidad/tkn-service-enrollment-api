package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CipherResponseDTO implements Serializable {

    private String value;

}