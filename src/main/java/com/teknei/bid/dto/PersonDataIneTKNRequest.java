package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class PersonDataIneTKNRequest implements Serializable {

    private Long id;
    private String rightIndexB64;
    private String leftIndexB64;

}