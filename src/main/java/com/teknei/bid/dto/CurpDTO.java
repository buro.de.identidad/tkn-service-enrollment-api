package com.teknei.bid.dto;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class CurpDTO implements Serializable {

    @NonNull
    private Long id;
    @NonNull
    private String curp;
    private String scanId;
    private String documentId;

}