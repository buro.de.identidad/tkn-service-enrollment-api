package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class SimpleDetailDTO implements Serializable {

    private Long id;

}