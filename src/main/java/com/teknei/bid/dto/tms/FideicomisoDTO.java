package com.teknei.bid.dto.tms;

import java.io.Serializable;

import lombok.Data;

@Data
public class FideicomisoDTO implements Serializable {

	String patrimonial;
	String beneficiario;

}

