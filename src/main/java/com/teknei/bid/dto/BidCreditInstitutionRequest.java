package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidCreditInstitutionRequest implements Serializable {

    private String name;
    private String desc;
    private String username;
    private Long id;
    private Boolean active;

}