package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsValidationResultDTO implements Serializable {

    private Boolean result;
    private Integer status;

}