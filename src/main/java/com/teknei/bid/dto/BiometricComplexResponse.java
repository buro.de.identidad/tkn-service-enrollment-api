package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BiometricComplexResponse implements Serializable {

    private String id;
    private String rightIndex;
    private String leftIndex;

}