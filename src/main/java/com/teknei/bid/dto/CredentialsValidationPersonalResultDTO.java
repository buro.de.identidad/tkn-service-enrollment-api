package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsValidationPersonalResultDTO implements Serializable {

    private Boolean result;
    private Integer status;
    private String name;
    private String lastnameFirst;
    private String lastnameLast;

}