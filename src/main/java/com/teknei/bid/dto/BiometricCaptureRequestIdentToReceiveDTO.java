package com.teknei.bid.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = {"id", "serial"})
public class BiometricCaptureRequestIdentToReceiveDTO implements Serializable {

    private Long id;
    private String serial;
    private String username;

}