package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClieQRDTO implements Serializable {

    private String qr;
    private String obs;
    private String username;

}