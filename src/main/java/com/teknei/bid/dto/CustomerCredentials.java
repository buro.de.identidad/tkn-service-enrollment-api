package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CustomerCredentials implements Serializable {

    private Long id;
    private String password;
    private String obs;

}